namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MovCaja")]
    public partial class MovCaja
    {
        public long Id { get; set; }

        public long IdCorteCaja { get; set; }

        public long IdTipoMovCaja { get; set; }

        public DateTime Fecha { get; set; }

        [StringLength(20)]
        public string Numero { get; set; }

        [StringLength(200)]
        public string Concepto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Monto { get; set; }

        public long IdCuentaBanco { get; set; }

        public long IdEmpresa { get; set; }

        public virtual CorteCaja CorteCaja { get; set; }

        public virtual TipoMovCaja TipoMovCaja { get; set; }
    }
}
