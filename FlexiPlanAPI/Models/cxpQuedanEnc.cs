namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cxpQuedanEnc")]
    public partial class cxpQuedanEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cxpQuedanEnc()
        {
            cxpQuedanDet = new HashSet<cxpQuedanDet>();
        }

        public long Id { get; set; }

        public long IdSucursal { get; set; }

        public long IdCliente { get; set; }

        public long IdPeriodo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Numero { get; set; }

        public DateTime Fecha { get; set; }

        public DateTime FechaPago { get; set; }

        public bool Cancelado { get; set; }

        public bool Anulado { get; set; }

        public long IdUsuario { get; set; }

        public bool? Impreso { get; set; }

        public long IdPlanImpuesto { get; set; }

        public long IdEmpresa { get; set; }

        public virtual Cliente Cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cxpQuedanDet> cxpQuedanDet { get; set; }

        public virtual Periodo Periodo { get; set; }

        public virtual Sucursal Sucursal { get; set; }
    }
}
