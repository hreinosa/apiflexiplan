namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FTLog")]
    public partial class FTLog
    {
        public long id { get; set; }

        public long idusuario { get; set; }

        public long idopcion { get; set; }

        [Required]
        [StringLength(25)]
        public string accion { get; set; }

        public DateTime stampa { get; set; }

        [StringLength(60)]
        public string entidad { get; set; }

        [StringLength(50)]
        public string formulario { get; set; }

        [StringLength(50)]
        public string tabla { get; set; }

        public bool Revisado { get; set; }

        public long IdEmpresa { get; set; }
    }
}
