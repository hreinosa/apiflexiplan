namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BaseDetalleImpuesto")]
    public partial class BaseDetalleImpuesto
    {
        public long Id { get; set; }

        [Column("BaseDetalleImpuesto")]
        [Required]
        [StringLength(50)]
        public string BaseDetalleImpuesto1 { get; set; }
    }
}
