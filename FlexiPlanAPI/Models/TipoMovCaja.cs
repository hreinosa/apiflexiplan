namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoMovCaja")]
    public partial class TipoMovCaja
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoMovCaja()
        {
            MovCaja = new HashSet<MovCaja>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(30)]
        public string TipoMovCajaDes { get; set; }

        public short Signo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MovCaja> MovCaja { get; set; }
    }
}
