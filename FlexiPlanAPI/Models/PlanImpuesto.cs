namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PlanImpuesto")]
    public partial class PlanImpuesto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PlanImpuesto()
        {
            Cliente = new HashSet<Cliente>();
            PlanXDetalleImpuesto = new HashSet<PlanXDetalleImpuesto>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(10)]
        public string CodPlanImpuesto { get; set; }

        [Required]
        [StringLength(60)]
        public string DesPlanImpuesto { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cliente> Cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlanXDetalleImpuesto> PlanXDetalleImpuesto { get; set; }
    }
}
