namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvMovEncImpuesto")]
    public partial class InvMovEncImpuesto
    {
        public long Id { get; set; }

        public long IdInvMovEnc { get; set; }

        public long IdInvMovDet { get; set; }

        public short Secuencia { get; set; }

        public long IdDetalleImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoVenta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoGraMin { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoGraMax { get; set; }

        public bool ImprimirEnDocumento { get; set; }

        public long IdcxpQuedanDet { get; set; }

        public long IdCompra { get; set; }

        public long IdVenta { get; set; }

        public long IdEmpresa { get; set; }

        public long IdPlanImpuesto { get; set; }

        public long IdInvProducto { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdUsuario { get; set; }
    }
}
