namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SaldoCC")]
    public partial class SaldoCC
    {
        public long Id { get; set; }

        public long IdPeriodo { get; set; }

        public long IdCatalogoCC { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoInicial { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoCargo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoAbono { get; set; }

        public long IdEmpresa { get; set; }

        public virtual CatalogoCC CatalogoCC { get; set; }

        public virtual Periodo Periodo { get; set; }
    }
}
