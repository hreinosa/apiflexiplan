namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Moneda")]
    public partial class Moneda
    {
        public long Id { get; set; }

        [Column("Moneda")]
        [Required]
        [StringLength(30)]
        public string Moneda1 { get; set; }

        [StringLength(30)]
        public string NomSingular { get; set; }

        [StringLength(30)]
        public string NomPlural { get; set; }

        [Required]
        [StringLength(5)]
        public string Simbolo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal EquivalenciaDolar { get; set; }

        [Column(TypeName = "numeric")]
        public decimal EquivalenciaEuro { get; set; }
    }
}
