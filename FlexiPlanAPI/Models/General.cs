namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("General")]
    public partial class General
    {
        public long Id { get; set; }

        [Required]
        [StringLength(1)]
        public string Codigo { get; set; }

        [Column("General")]
        [Required]
        [StringLength(30)]
        public string General1 { get; set; }
    }
}
