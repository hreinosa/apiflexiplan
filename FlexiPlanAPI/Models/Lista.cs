namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Lista")]
    public partial class Lista
    {
        public long? id { get; set; }

        public long? idpadre { get; set; }

        [StringLength(255)]
        public string opciontipo { get; set; }

        [StringLength(255)]
        public string opciondes { get; set; }

        [StringLength(255)]
        public string programa { get; set; }

        [StringLength(255)]
        public string vbindex { get; set; }

        [StringLength(255)]
        public string Posicion { get; set; }

        [StringLength(255)]
        public string Ico { get; set; }

        [Key]
        public bool activo { get; set; }

        [StringLength(255)]
        public string filtro { get; set; }

        [StringLength(8000)]
        public string SeleccionSql { get; set; }
    }
}
