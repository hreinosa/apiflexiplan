namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CredNoEnc")]
    public partial class CredNoEnc
    {
        public long ID { get; set; }

        public long IdCortePago { get; set; }

        [Required]
        [StringLength(10)]
        public string CodCliente { get; set; }

        [Required]
        [StringLength(20)]
        public string CodCredito { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Total { get; set; }

        public virtual CortePago CortePago { get; set; }
    }
}
