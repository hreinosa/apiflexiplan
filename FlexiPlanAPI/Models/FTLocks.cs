namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FTLocks
    {
        [Key]
        [Column(Order = 0)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Tabla { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdReg { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(200)]
        public string Opcion { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime Stamp { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(20)]
        public string Usuario { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdEmpresa { get; set; }
    }
}
