namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvMarca")]
    public partial class InvMarca
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvMarca()
        {
            InvProducto = new HashSet<InvProducto>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        public string InvMarcaDes { get; set; }

        public bool MultiMarca { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvProducto> InvProducto { get; set; }
    }
}
