namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccesoUsuario")]
    public partial class AccesoUsuario
    {
        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdUsuario { get; set; }

        public bool VerDataEmpleado { get; set; }

        public long IdPeriodoSeleccionado { get; set; }

        public bool Acceso { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
