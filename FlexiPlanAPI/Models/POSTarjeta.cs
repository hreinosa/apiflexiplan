namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("POSTarjeta")]
    public partial class POSTarjeta
    {
        public long Id { get; set; }

        [Required]
        [StringLength(40)]
        public string POSTarjetaDes { get; set; }

        public bool Automatico { get; set; }

        public long IdEmpresa { get; set; }
    }
}
