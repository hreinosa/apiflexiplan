namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvListaPrecioDet")]
    public partial class InvListaPrecioDet
    {
        public long Id { get; set; }

        public long IdInvListaPrecioEnc { get; set; }

        public long IdInvMetodoPrecio { get; set; }

        public long IdInvNivelPrecio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Porcentaje { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Precio { get; set; }

        public long IdInvMedida { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CantidadInicial { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CantidadFinal { get; set; }

        public long IdEmpresa { get; set; }

        public bool IncluyeImpuesto { get; set; }

        public long IdDetalleImpuesto { get; set; }

        public virtual InvListaPrecioEnc InvListaPrecioEnc { get; set; }
    }
}
