namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvProducto")]
    public partial class InvProducto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvProducto()
        {
            InvListaPrecioEnc = new HashSet<InvListaPrecioEnc>();
            InvMovDet = new HashSet<InvMovDet>();
            InvProductoPartes = new HashSet<InvProductoPartes>();
        }

        public long Id { get; set; }

        public long IdInvClase { get; set; }

        public long IdInvDivision { get; set; }

        public long IdInvMarca { get; set; }

        public long IdInvModelo { get; set; }

        public long? IdProveedor { get; set; }

        public long IdInvGrupo { get; set; }

        public long IdInvClasif { get; set; }

        public long? IdInvMedida { get; set; }

        [Required]
        [StringLength(25)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(200)]
        public string InvProductoDes { get; set; }

        public bool Serie { get; set; }

        public bool Exento { get; set; }

        public bool Combo { get; set; }

        public bool Servicio { get; set; }

        public bool Consignacion { get; set; }

        [StringLength(100)]
        public string NomComercial { get; set; }

        [StringLength(100)]
        public string ConocidoPor { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Costo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Existencia { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdCatalogo3 { get; set; }

        public long IdCatalogo4 { get; set; }

        public long IdCatalogo5 { get; set; }

        public long IdCatalogo6 { get; set; }

        public long IdCatalogo7 { get; set; }

        public long IdCatalogo8 { get; set; }

        public virtual InvClase InvClase { get; set; }

        public virtual InvClasif InvClasif { get; set; }

        public virtual InvGrupo InvGrupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvListaPrecioEnc> InvListaPrecioEnc { get; set; }

        public virtual InvMarca InvMarca { get; set; }

        public virtual InvMedida InvMedida { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvMovDet> InvMovDet { get; set; }

        public virtual InvRubro InvRubro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvProductoPartes> InvProductoPartes { get; set; }
    }
}
