namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CortePago")]
    public partial class CortePago
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CortePago()
        {
            Asociacion = new HashSet<Asociacion>();
            CreditoEnc = new HashSet<CreditoEnc>();
            CredNoEnc = new HashSet<CredNoEnc>();
            Inactivos = new HashSet<Inactivos>();
            SolicitudEnc = new HashSet<SolicitudEnc>();
        }

        public long Id { get; set; }

        public DateTime FechaPago { get; set; }

        public DateTime? FechaCorte { get; set; }

        public DateTime? FechaEnvio { get; set; }

        public bool Enviado { get; set; }

        public bool Recibido { get; set; }

        public bool Procesado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asociacion> Asociacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditoEnc> CreditoEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CredNoEnc> CredNoEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inactivos> Inactivos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudEnc> SolicitudEnc { get; set; }
    }
}
