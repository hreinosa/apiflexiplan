namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Config")]
    public partial class Config
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Nombre { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdEmpresa { get; set; }

        [StringLength(65)]
        public string Descripcion { get; set; }

        [Required]
        [StringLength(1)]
        public string Tipo { get; set; }

        [Required]
        [StringLength(200)]
        public string Valor { get; set; }

        [StringLength(10)]
        public string Posicion { get; set; }

        public bool Inactivo { get; set; }

        public bool Visible { get; set; }

        public bool Editable { get; set; }
    }
}
