namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DiarioDet")]
    public partial class DiarioDet
    {
        public long Id { get; set; }

        public long IdDiarioEnc { get; set; }

        public long IdCatalogo { get; set; }

        public short Secuencia { get; set; }

        [StringLength(600)]
        public string Concepto { get; set; }

        [StringLength(20)]
        public string Documento { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Cargo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Abono { get; set; }

        public long IdExtra { get; set; }

        [StringLength(1)]
        public string TipoExtra { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCatalogoCC { get; set; }

        public virtual Catalogo Catalogo { get; set; }

        public virtual DiarioEnc DiarioEnc { get; set; }
    }
}
