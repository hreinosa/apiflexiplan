namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("invactivodet")]
    public partial class invactivodet
    {
        public long id { get; set; }

        public long idinvactivoenc { get; set; }

        public long idactivofijo { get; set; }

        public bool? existente { get; set; }

        public bool? nuevo { get; set; }

        public long IdEmpresa { get; set; }
    }
}
