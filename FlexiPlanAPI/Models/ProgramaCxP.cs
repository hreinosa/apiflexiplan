namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProgramaCxP")]
    public partial class ProgramaCxP
    {
        public long Id { get; set; }

        public long IdCxpQuedanDet { get; set; }

        public DateTime Fecha { get; set; }

        public bool Programado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorProgramado { get; set; }

        public bool Autorizado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorAutorizado { get; set; }

        public long? IdPrograma { get; set; }

        public long? IdAutoriza { get; set; }

        public long Cerrado { get; set; }

        public long IdCuentaBanco { get; set; }

        public long IdEmpresa { get; set; }

        public virtual cxpQuedanDet cxpQuedanDet { get; set; }
    }
}
