namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvTipoDocumento")]
    public partial class InvTipoDocumento
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        public string InvTipoDocumentoDes { get; set; }

        public short Signo { get; set; }

        public bool Postear { get; set; }

        public long IdInvTipoMov { get; set; }
    }
}
