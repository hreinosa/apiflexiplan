namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CreditoDet")]
    public partial class CreditoDet
    {
        public long Id { get; set; }

        public long IdCreditoEnc { get; set; }

        public long IdTipoAbono { get; set; }

        public short Secuencia { get; set; }

        public DateTime Fecha { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Total { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CapitalPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CapVenPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CapitalNoPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal InteresPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal IntVenPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal InteresNoPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SeguroPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SeguroNoPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MoraPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MoraVenPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MoraNoPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaInteres { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaSeguro { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaMora { get; set; }

        public DateTime? FecAnt { get; set; }

        public DateTime? FecProx { get; set; }

        public long IdCortePago { get; set; }

        public virtual CreditoEnc CreditoEnc { get; set; }

        public virtual TipoAbono TipoAbono { get; set; }
    }
}
