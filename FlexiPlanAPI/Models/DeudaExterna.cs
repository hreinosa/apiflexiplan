namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DeudaExterna")]
    public partial class DeudaExterna
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeudaExterna()
        {
            SolicitudExt = new HashSet<SolicitudExt>();
        }

        public long Id { get; set; }

        public long IdTipoExterno { get; set; }

        public long IdCliente { get; set; }

        [StringLength(30)]
        public string Referencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoInicial { get; set; }

        [Column(TypeName = "numeric")]
        public decimal NumeroCuotas { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorCuota { get; set; }

        [Required]
        [StringLength(1)]
        public string Frecuencia { get; set; }

        public bool Activo { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual TipoExterno TipoExterno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudExt> SolicitudExt { get; set; }
    }
}
