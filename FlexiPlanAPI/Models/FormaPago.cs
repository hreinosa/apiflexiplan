namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormaPago")]
    public partial class FormaPago
    {
        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        public string FormaPagoDes { get; set; }

        public short Plazo { get; set; }

        public bool Contado { get; set; }

        public bool Credito { get; set; }

        public bool NoAplica { get; set; }
    }
}
