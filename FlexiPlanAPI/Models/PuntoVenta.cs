namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PuntoVenta")]
    public partial class PuntoVenta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PuntoVenta()
        {
            CorteCaja = new HashSet<CorteCaja>();
            InvDescEnc = new HashSet<InvDescEnc>();
        }

        public long Id { get; set; }

        public long IdSucursal { get; set; }

        [Required]
        [StringLength(60)]
        public string PuntoVentaDes { get; set; }

        public bool Predeterminado { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CorteCaja> CorteCaja { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvDescEnc> InvDescEnc { get; set; }

        public virtual Sucursal Sucursal { get; set; }
    }
}
