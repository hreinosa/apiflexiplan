namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PresupConfigDet")]
    public partial class PresupConfigDet
    {
        public long Id { get; set; }

        public long IdPresupConfigEnc { get; set; }

        public long IdCatalogo { get; set; }

        public short Secuencia { get; set; }

        public long IdEmpresa { get; set; }

        public virtual Catalogo Catalogo { get; set; }

        public virtual PresupConfigEnc PresupConfigEnc { get; set; }
    }
}
