namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvListaPrecioEnc")]
    public partial class InvListaPrecioEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvListaPrecioEnc()
        {
            InvListaPrecioDet = new HashSet<InvListaPrecioDet>();
        }

        public long Id { get; set; }

        public long IdInvProducto { get; set; }

        public long IdInvMetodoPrecio { get; set; }

        public long IdInvNivelPrecio { get; set; }

        public long IdEmpresa { get; set; }

        public long IdInvMedida { get; set; }

        public bool IncluyeImpuesto { get; set; }

        public long IdDetalleImpuesto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvListaPrecioDet> InvListaPrecioDet { get; set; }

        public virtual InvProducto InvProducto { get; set; }
    }
}
