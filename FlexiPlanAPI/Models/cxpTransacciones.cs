namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cxpTransacciones
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cxpTransacciones()
        {
            cxpTransaccionesAplicadas = new HashSet<cxpTransaccionesAplicadas>();
        }

        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdInvMovEnc { get; set; }

        public long IdDocumento { get; set; }

        public long IdInvTipoDocumento { get; set; }

        [StringLength(20)]
        public string Numero { get; set; }

        public long IdcxpTipoTransaccion { get; set; }

        [Required]
        [StringLength(20)]
        public string NumTransaccion { get; set; }

        public long IdCliente { get; set; }

        public long IdSucursal { get; set; }

        public long IdPeriodo { get; set; }

        public long IdUsuario { get; set; }

        public DateTime Fecha { get; set; }

        public short Plazo { get; set; }

        public long IdQuedan { get; set; }

        [StringLength(20)]
        public string NumeroQuedan { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoDocumento { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoAbonado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoSaldo { get; set; }

        public DateTime? FechaPago { get; set; }

        public short FormaPago { get; set; }

        public long IdCuentaBanco { get; set; }

        public long IdTarjeta { get; set; }

        [StringLength(20)]
        public string NumeroChequeTarjeta { get; set; }

        public short Signo { get; set; }

        [StringLength(30)]
        public string Origen { get; set; }

        public bool Anulado { get; set; }

        public bool Contabilizado { get; set; }

        public long IdDiarioEnc { get; set; }

        [StringLength(200)]
        public string Concepto { get; set; }

        public long IdMovimientoBanco { get; set; }

        public long IdTipoMovBanco { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cxpTransaccionesAplicadas> cxpTransaccionesAplicadas { get; set; }
    }
}
