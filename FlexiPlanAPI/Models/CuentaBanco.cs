namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CuentaBanco")]
    public partial class CuentaBanco
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CuentaBanco()
        {
            BanConciliacion = new HashSet<BanConciliacion>();
            MovimientoBanco = new HashSet<MovimientoBanco>();
            SaldoBanco = new HashSet<SaldoBanco>();
        }

        public long Id { get; set; }

        public long IdBanco { get; set; }

        public long IdCatalogo { get; set; }

        [Required]
        [StringLength(30)]
        public string Numero { get; set; }

        public long Ultimo { get; set; }

        [StringLength(60)]
        public string Descripcion { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Saldo { get; set; }

        public long? IdMoneda { get; set; }

        public long IdEmpresa { get; set; }

        public virtual Banco Banco { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BanConciliacion> BanConciliacion { get; set; }

        public virtual Catalogo Catalogo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MovimientoBanco> MovimientoBanco { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaldoBanco> SaldoBanco { get; set; }
    }
}
