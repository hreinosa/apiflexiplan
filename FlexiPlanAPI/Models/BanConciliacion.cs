namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BanConciliacion")]
    public partial class BanConciliacion
    {
        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdPeriodo { get; set; }

        public long IdCuentaBanco { get; set; }

        public DateTime Fecha { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoBanco { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoCuentaBanco { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoConciliacion { get; set; }

        public virtual CuentaBanco CuentaBanco { get; set; }

        public virtual Periodo Periodo { get; set; }
    }
}
