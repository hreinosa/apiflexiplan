namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrdenarCompra")]
    public partial class OrdenarCompra
    {
        public long Id { get; set; }

        public long IdInvMovDet { get; set; }

        public long IdCliente { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Costo { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
