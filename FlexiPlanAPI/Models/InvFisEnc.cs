namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvFisEnc")]
    public partial class InvFisEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvFisEnc()
        {
            InvFisDet = new HashSet<InvFisDet>();
        }

        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdPeriodo { get; set; }

        public long IdSucursal { get; set; }

        public long IdUsuario { get; set; }

        [Required]
        [StringLength(15)]
        public string Numero { get; set; }

        public DateTime Fecha { get; set; }

        public DateTime? FechaCierre { get; set; }

        public bool Cerrado { get; set; }

        public long IdInvClase { get; set; }

        public long IdInvProducto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvFisDet> InvFisDet { get; set; }

        public virtual Periodo Periodo { get; set; }

        public virtual Sucursal Sucursal { get; set; }
    }
}
