namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Division")]
    public partial class Division
    {
        public long Id { get; set; }

        [Required]
        [StringLength(1)]
        public string Codigo { get; set; }

        [Column("Division")]
        [Required]
        [StringLength(30)]
        public string Division1 { get; set; }

        public long IdEmpresa { get; set; }
    }
}
