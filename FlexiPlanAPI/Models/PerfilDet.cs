namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PerfilDet")]
    public partial class PerfilDet
    {
        public long id { get; set; }

        public long idPerfilEnc { get; set; }

        public long idOpcion { get; set; }

        public bool visible { get; set; }

        public bool adicion { get; set; }

        public bool edicion { get; set; }

        public bool eliminacion { get; set; }

        public bool impresion { get; set; }

        public long IdEmpresa { get; set; }

        public virtual PerfilEnc PerfilEnc { get; set; }
    }
}
