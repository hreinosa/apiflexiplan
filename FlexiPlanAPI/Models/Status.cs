namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Status
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Status()
        {
            CreditoEnc = new HashSet<CreditoEnc>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        public string StatusDes { get; set; }

        public bool EsActivo { get; set; }

        public bool EsMora { get; set; }

        public bool EsCerrado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditoEnc> CreditoEnc { get; set; }
    }
}
