namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Deudor")]
    public partial class Deudor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Deudor()
        {
            SolicitudDet = new HashSet<SolicitudDet>();
            SolicitudExt = new HashSet<SolicitudExt>();
        }

        public long Id { get; set; }

        public long IdSolicitud { get; set; }

        public long IdCliente { get; set; }

        public long IdLugarTrabajo { get; set; }

        public bool Principal { get; set; }

        public bool DeudorActual { get; set; }

        public bool CalcularDeuda { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Ahorrado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Indem { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual LugarTrabajo LugarTrabajo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudDet> SolicitudDet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudExt> SolicitudExt { get; set; }
    }
}
