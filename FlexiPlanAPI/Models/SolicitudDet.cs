namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SolicitudDet")]
    public partial class SolicitudDet
    {
        public long Id { get; set; }

        public long IdDeudor { get; set; }

        public long IdCreditoEnc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CuotaCredito { get; set; }

        [Required]
        [StringLength(1)]
        public string Frecuencia { get; set; }

        public DateTime FechaSaldo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Saldo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoDescontar { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Interes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Seguro { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Mora { get; set; }

        public bool Descontar { get; set; }

        public virtual CreditoEnc CreditoEnc { get; set; }

        public virtual Deudor Deudor { get; set; }
    }
}
