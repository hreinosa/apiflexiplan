namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Ciudades
    {
        public long Id { get; set; }

        public long IdEstados { get; set; }

        [Required]
        [StringLength(4)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        public string CiudadesDes { get; set; }
    }
}
