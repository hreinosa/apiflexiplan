namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LugarTrabajo")]
    public partial class LugarTrabajo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LugarTrabajo()
        {
            Deudor = new HashSet<Deudor>();
        }

        public long Id { get; set; }

        public int Codigo { get; set; }

        [StringLength(40)]
        public string LugarTrabajoDes { get; set; }

        public bool EsProv { get; set; }

        public bool EsPart { get; set; }

        public bool EsAcreedor { get; set; }

        public bool EsEmpleado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Deudor> Deudor { get; set; }
    }
}
