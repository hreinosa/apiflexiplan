namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Asociacion")]
    public partial class Asociacion
    {
        public long Id { get; set; }

        public long IdCliente { get; set; }

        public long IdCortePago { get; set; }

        [Column(TypeName = "numeric")]
        public decimal AhorroAnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal AhorroNuevo { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual CortePago CortePago { get; set; }
    }
}
