namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Documento")]
    public partial class Documento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Documento()
        {
            cxpQuedanDet = new HashSet<cxpQuedanDet>();
            InvMovEnc = new HashSet<InvMovEnc>();
        }

        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdInvTipoDocumento { get; set; }

        public long IdInvTipoMov { get; set; }

        [Required]
        [StringLength(50)]
        public string DocumentoDes { get; set; }

        [StringLength(10)]
        public string NomCorto { get; set; }

        public byte MaxLinea { get; set; }

        [StringLength(20)]
        public string Correlativo { get; set; }

        [StringLength(1000)]
        public string Formulario { get; set; }

        [StringLength(10)]
        public string Serie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cxpQuedanDet> cxpQuedanDet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvMovEnc> InvMovEnc { get; set; }
    }
}
