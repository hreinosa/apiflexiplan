namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PresupConfigEnc")]
    public partial class PresupConfigEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PresupConfigEnc()
        {
            PresupConfigDet = new HashSet<PresupConfigDet>();
        }

        public long Id { get; set; }

        public short Secuencia { get; set; }

        [Required]
        [StringLength(100)]
        public string Concepto { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PresupConfigDet> PresupConfigDet { get; set; }
    }
}
