namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DiarioEnc")]
    public partial class DiarioEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DiarioEnc()
        {
            cxpQuedanDet = new HashSet<cxpQuedanDet>();
            DiarioDet = new HashSet<DiarioDet>();
        }

        public long Id { get; set; }

        public long IdTipoPartida { get; set; }

        public long IdPeriodo { get; set; }

        public long IdUsuario { get; set; }

        public int Numero { get; set; }

        public DateTime Fecha { get; set; }

        [StringLength(600)]
        public string Concepto { get; set; }

        public long IdEmpresa { get; set; }

        [StringLength(30)]
        public string Origen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cxpQuedanDet> cxpQuedanDet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiarioDet> DiarioDet { get; set; }

        public virtual Periodo Periodo { get; set; }

        public virtual TipoPartida TipoPartida { get; set; }
    }
}
