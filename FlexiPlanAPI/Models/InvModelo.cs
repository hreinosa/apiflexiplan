namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvModelo")]
    public partial class InvModelo
    {
        public long Id { get; set; }

        [Required]
        [StringLength(30)]
        public string InvModeloDes { get; set; }

        public long IdEmpresa { get; set; }
    }
}
