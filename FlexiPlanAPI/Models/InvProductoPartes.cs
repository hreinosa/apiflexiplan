namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class InvProductoPartes
    {
        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdInvProducto { get; set; }

        public long IdSucursal { get; set; }

        [StringLength(50)]
        public string Modelo { get; set; }

        [StringLength(50)]
        public string NumeroParte { get; set; }

        [StringLength(50)]
        public string NumeroSerie { get; set; }

        [StringLength(50)]
        public string CodigoBarra1 { get; set; }

        [StringLength(50)]
        public string CodigoBarra2 { get; set; }

        public long IdUsuario { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Existencia { get; set; }

        public virtual InvProducto InvProducto { get; set; }
    }
}
