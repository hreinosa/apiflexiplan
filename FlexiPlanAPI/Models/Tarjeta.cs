namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tarjeta")]
    public partial class Tarjeta
    {
        public long Id { get; set; }

        [Required]
        [StringLength(40)]
        public string TarjetaDes { get; set; }

        public bool Debito { get; set; }

        public long IdEmpresa { get; set; }
    }
}
