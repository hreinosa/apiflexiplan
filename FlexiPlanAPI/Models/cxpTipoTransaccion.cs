namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cxpTipoTransaccion")]
    public partial class cxpTipoTransaccion
    {
        public long Id { get; set; }

        [Column("cxpTipoTransaccion")]
        [Required]
        [StringLength(20)]
        public string cxpTipoTransaccion1 { get; set; }

        public short Signo { get; set; }

        [Required]
        [StringLength(20)]
        public string Numero { get; set; }
    }
}
