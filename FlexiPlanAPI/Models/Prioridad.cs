namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Prioridad")]
    public partial class Prioridad
    {
        public long Id { get; set; }

        [Required]
        [StringLength(50)]
        public string PrioridadDes { get; set; }

        public short TiempoHora { get; set; }

        public bool Emergencia { get; set; }

        public bool Urgente { get; set; }

        public bool Normal { get; set; }
    }
}
