namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Idioma")]
    public partial class Idioma
    {
        public long ID { get; set; }

        [Required]
        [StringLength(10)]
        public string IdiomaDes { get; set; }

        [Required]
        [StringLength(10)]
        public string IdiomaCol { get; set; }
    }
}
