namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Traduccion")]
    public partial class Traduccion
    {
        public long ID { get; set; }

        [Required]
        [StringLength(30)]
        public string Formulario { get; set; }

        [Required]
        [StringLength(40)]
        public string Control { get; set; }

        [Required]
        [StringLength(100)]
        public string Espanol { get; set; }

        [Required]
        [StringLength(100)]
        public string Ingles { get; set; }

        public bool RotuloCaption { get; set; }

        public bool RotuloToolTipText { get; set; }

        public bool RotuloReporte { get; set; }

        public bool Messagebox { get; set; }
    }
}
