namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CxpTransaccionesCentroCosto")]
    public partial class CxpTransaccionesCentroCosto
    {
        public long Id { get; set; }

        public long IdCxpTransacciones { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCuenta { get; set; }

        public long IdCuentaCC { get; set; }

        [Required]
        [StringLength(20)]
        public string CodigoCuentaCC { get; set; }

        [StringLength(100)]
        public string DescripcionCuentaCC { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Monto { get; set; }

        public bool Contabilizado { get; set; }

        public long IdUsuario { get; set; }

        [StringLength(600)]
        public string Concepto { get; set; }

        [StringLength(20)]
        public string Documento { get; set; }

        public long IdOpcion { get; set; }
    }
}
