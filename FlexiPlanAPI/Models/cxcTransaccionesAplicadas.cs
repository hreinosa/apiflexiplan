namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cxcTransaccionesAplicadas
    {
        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdUsuario { get; set; }

        public long IdCliente { get; set; }

        public long IdcxcTransaccionAplicar { get; set; }

        public long IdcxcTipoTransaccionAplicar { get; set; }

        [Required]
        [StringLength(20)]
        public string NumeroAplicar { get; set; }

        public long IdcxcTransaccionAplicado { get; set; }

        public long IdcxcTipoTransaccionAplicado { get; set; }

        [Required]
        [StringLength(20)]
        public string NumeroAplicado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoAplicar { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoAplicado { get; set; }

        public short Signo { get; set; }

        [StringLength(30)]
        public string Origen { get; set; }

        public bool Aplicado { get; set; }

        public virtual cxcTransacciones cxcTransacciones { get; set; }
    }
}
