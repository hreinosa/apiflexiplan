namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ClasificaCliente")]
    public partial class ClasificaCliente
    {
        public long Id { get; set; }

        [Required]
        [StringLength(25)]
        public string ClasificaDes { get; set; }

        public bool MuyBueno { get; set; }

        public bool Bueno { get; set; }

        public bool Regular { get; set; }
    }
}
