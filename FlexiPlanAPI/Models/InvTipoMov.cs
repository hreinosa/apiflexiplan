namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvTipoMov")]
    public partial class InvTipoMov
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvTipoMov()
        {
            InvMovEnc = new HashSet<InvMovEnc>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(30)]
        public string InvTipoMovDes { get; set; }

        public short Signo { get; set; }

        public long IdTipoPartida { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvMovEnc> InvMovEnc { get; set; }
    }
}
