namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConPresupuestoDet")]
    public partial class ConPresupuestoDet
    {
        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdConPresupuestoEnc { get; set; }

        public long IdPeriodo { get; set; }

        public long IdTipoCuenta { get; set; }

        public long IdCatalogo { get; set; }

        public int Secuencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Presupuestado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Ejecutado { get; set; }
    }
}
