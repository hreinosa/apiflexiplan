namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoActivo")]
    public partial class TipoActivo
    {
        public long id { get; set; }

        [Required]
        [StringLength(20)]
        public string TipoActivodes { get; set; }
    }
}
