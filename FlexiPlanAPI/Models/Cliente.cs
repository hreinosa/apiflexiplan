namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cliente")]
    public partial class Cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cliente()
        {
            Asociacion = new HashSet<Asociacion>();
            CreditoEnc = new HashSet<CreditoEnc>();
            cxpQuedanEnc = new HashSet<cxpQuedanEnc>();
            DeudaExterna = new HashSet<DeudaExterna>();
            Deudor = new HashSet<Deudor>();
            Inactivos = new HashSet<Inactivos>();
            InvMovEnc = new HashSet<InvMovEnc>();
            OrdenarCompra = new HashSet<OrdenarCompra>();
            Parientes = new HashSet<Parientes>();
            Proyecto = new HashSet<Proyecto>();
            RetiroEmpleado = new HashSet<RetiroEmpleado>();
        }

        public long Id { get; set; }

        public long IdLugarTrabajo { get; set; }

        [Required]
        [StringLength(10)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        public bool Empleado { get; set; }

        public bool Socio { get; set; }

        public bool Bloqueado { get; set; }

        public DateTime? FechaIngresoEmp { get; set; }

        public DateTime? FechaIngresoAsoc { get; set; }

        public DateTime? FechaRetiroAsoc { get; set; }

        public DateTime? FechaRetiroEmp { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Sueldo { get; set; }

        [StringLength(200)]
        public string Direccion { get; set; }

        [StringLength(20)]
        public string DUI { get; set; }

        [StringLength(20)]
        public string NIT { get; set; }

        [StringLength(15)]
        public string NRC { get; set; }

        [StringLength(20)]
        public string Telefono { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        [StringLength(60)]
        public string eMail { get; set; }

        public bool Generico { get; set; }

        [Required]
        [StringLength(1)]
        public string Sexo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PorAhorro { get; set; }

        public DateTime? FechaNacimiento { get; set; }

        public long? IdBanco { get; set; }

        [StringLength(40)]
        public string NumCuenta { get; set; }

        [StringLength(20)]
        public string TipoCuenta { get; set; }

        [StringLength(400)]
        public string WebSite { get; set; }

        [StringLength(200)]
        public string Giro { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Ganancia { get; set; }

        [StringLength(255)]
        public string ArchivoFoto { get; set; }

        public short DiasCredito { get; set; }

        public bool Credito { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoCredito { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SobreGiro { get; set; }

        [StringLength(30)]
        public string Cargo { get; set; }

        public long IdClasifica { get; set; }

        [StringLength(60)]
        public string Contacto { get; set; }

        public long IdPlanImpuesto { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdCatalogo3 { get; set; }

        public long IdCatalogo4 { get; set; }

        public long IdCatalogoCC { get; set; }

        public long IdDocumento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asociacion> Asociacion { get; set; }

        public virtual PlanImpuesto PlanImpuesto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditoEnc> CreditoEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cxpQuedanEnc> cxpQuedanEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeudaExterna> DeudaExterna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Deudor> Deudor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inactivos> Inactivos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvMovEnc> InvMovEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrdenarCompra> OrdenarCompra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Parientes> Parientes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proyecto> Proyecto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RetiroEmpleado> RetiroEmpleado { get; set; }
    }
}
