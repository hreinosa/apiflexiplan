namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CriterioCotiza")]
    public partial class CriterioCotiza
    {
        public long Id { get; set; }

        [Required]
        [StringLength(50)]
        public string CriterioCotizaDes { get; set; }

        [StringLength(25)]
        public string Campo { get; set; }

        [StringLength(25)]
        public string CampoAmostrar { get; set; }

        public byte Orden { get; set; }

        public bool Activo { get; set; }

        public bool Ascendente { get; set; }
    }
}
