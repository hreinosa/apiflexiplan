namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Presupuesto")]
    public partial class Presupuesto
    {
        public long Id { get; set; }

        public long IdPeriodo { get; set; }

        public long IdCatalogo { get; set; }

        public int Secuencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Saldo { get; set; }

        public long IdEmpresa { get; set; }

        public virtual Catalogo Catalogo { get; set; }

        public virtual Periodo Periodo { get; set; }
    }
}
