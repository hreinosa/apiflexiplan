namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvPago")]
    public partial class InvPago
    {
        public long Id { get; set; }

        public long IdInvMovEnc { get; set; }

        public long IdTipoCredito { get; set; }

        public long? IdCortePago { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoPago { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoRec { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoCambio { get; set; }

        [Required]
        [StringLength(1)]
        public string Frecuencia { get; set; }

        public short Cuotas { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorCuota { get; set; }

        public long IdPosTarjeta { get; set; }

        public long IdBanco { get; set; }

        [StringLength(20)]
        public string NumTarjeta { get; set; }

        [StringLength(20)]
        public string NumCheque { get; set; }

        public long IdEmpresa { get; set; }

        public virtual TipoCredito TipoCredito { get; set; }
    }
}
