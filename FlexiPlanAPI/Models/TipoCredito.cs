namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoCredito")]
    public partial class TipoCredito
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoCredito()
        {
            CreditoEnc = new HashSet<CreditoEnc>();
            InvPago = new HashSet<InvPago>();
            SolicitudEnc = new HashSet<SolicitudEnc>();
        }

        public long Id { get; set; }

        public long IdCtaCredito { get; set; }

        public long IdCtaInteres { get; set; }

        public long IdCtaSeguro { get; set; }

        public long IdCtaMora { get; set; }

        [StringLength(40)]
        public string TipoCreditoDes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaInteres { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaSeguro { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaMora { get; set; }

        public short CodCampero { get; set; }

        public bool Ahorro { get; set; }

        public bool Tiempo { get; set; }

        public short Correlativo { get; set; }

        public bool EsCredito { get; set; }

        public bool Efectivo { get; set; }

        public bool Cheque { get; set; }

        public bool Tarjeta { get; set; }

        public virtual Catalogo Catalogo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditoEnc> CreditoEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvPago> InvPago { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudEnc> SolicitudEnc { get; set; }
    }
}
