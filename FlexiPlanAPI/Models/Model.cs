namespace FlexiPlanAPI.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=Modelo")
        {
        }

        public virtual DbSet<AccesoUsuario> AccesoUsuario { get; set; }
        public virtual DbSet<ActAniosUso> ActAniosUso { get; set; }
        public virtual DbSet<ActivoFijo> ActivoFijo { get; set; }
        public virtual DbSet<Asociacion> Asociacion { get; set; }
        public virtual DbSet<Banco> Banco { get; set; }
        public virtual DbSet<BanConciliacion> BanConciliacion { get; set; }
        public virtual DbSet<BaseDetalleImpuesto> BaseDetalleImpuesto { get; set; }
        public virtual DbSet<Catalogo> Catalogo { get; set; }
        public virtual DbSet<CatalogoCC> CatalogoCC { get; set; }
        public virtual DbSet<CatalogoOpcion> CatalogoOpcion { get; set; }
        public virtual DbSet<CatalogoxCatalogoCC> CatalogoxCatalogoCC { get; set; }
        public virtual DbSet<Categoria> Categoria { get; set; }
        public virtual DbSet<Ciudades> Ciudades { get; set; }
        public virtual DbSet<ClaseActivo> ClaseActivo { get; set; }
        public virtual DbSet<ClasifEFE> ClasifEFE { get; set; }
        public virtual DbSet<ClasificaCliente> ClasificaCliente { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Config> Config { get; set; }
        public virtual DbSet<ConPresupuestoDet> ConPresupuestoDet { get; set; }
        public virtual DbSet<ConPresupuestoEnc> ConPresupuestoEnc { get; set; }
        public virtual DbSet<CorteCaja> CorteCaja { get; set; }
        public virtual DbSet<CortePago> CortePago { get; set; }
        public virtual DbSet<CreditoDet> CreditoDet { get; set; }
        public virtual DbSet<CreditoEnc> CreditoEnc { get; set; }
        public virtual DbSet<CredNoEnc> CredNoEnc { get; set; }
        public virtual DbSet<CriterioCotiza> CriterioCotiza { get; set; }
        public virtual DbSet<CuentaBanco> CuentaBanco { get; set; }
        public virtual DbSet<cxcTipoTransaccion> cxcTipoTransaccion { get; set; }
        public virtual DbSet<cxcTransacciones> cxcTransacciones { get; set; }
        public virtual DbSet<cxcTransaccionesAplicadas> cxcTransaccionesAplicadas { get; set; }
        public virtual DbSet<CxcTransaccionesCentroCosto> CxcTransaccionesCentroCosto { get; set; }
        public virtual DbSet<CxcTransaccionesContable> CxcTransaccionesContable { get; set; }
        public virtual DbSet<cxpQuedanDet> cxpQuedanDet { get; set; }
        public virtual DbSet<cxpQuedanEnc> cxpQuedanEnc { get; set; }
        public virtual DbSet<cxpTipoTransaccion> cxpTipoTransaccion { get; set; }
        public virtual DbSet<cxpTransacciones> cxpTransacciones { get; set; }
        public virtual DbSet<cxpTransaccionesAplicadas> cxpTransaccionesAplicadas { get; set; }
        public virtual DbSet<CxpTransaccionesCentroCosto> CxpTransaccionesCentroCosto { get; set; }
        public virtual DbSet<CxpTransaccionesContable> CxpTransaccionesContable { get; set; }
        public virtual DbSet<Departamento> Departamento { get; set; }
        public virtual DbSet<Depreciacion> Depreciacion { get; set; }
        public virtual DbSet<DetalleImpuesto> DetalleImpuesto { get; set; }
        public virtual DbSet<DeudaExterna> DeudaExterna { get; set; }
        public virtual DbSet<Deudor> Deudor { get; set; }
        public virtual DbSet<DiarioDet> DiarioDet { get; set; }
        public virtual DbSet<DiarioEnc> DiarioEnc { get; set; }
        public virtual DbSet<Division> Division { get; set; }
        public virtual DbSet<Documento> Documento { get; set; }
        public virtual DbSet<Ejercicio> Ejercicio { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<EstadoCivil> EstadoCivil { get; set; }
        public virtual DbSet<Estados> Estados { get; set; }
        public virtual DbSet<EstResConfig> EstResConfig { get; set; }
        public virtual DbSet<EstResultado> EstResultado { get; set; }
        public virtual DbSet<FormaPago> FormaPago { get; set; }
        public virtual DbSet<FTLog> FTLog { get; set; }
        public virtual DbSet<General> General { get; set; }
        public virtual DbSet<Idioma> Idioma { get; set; }
        public virtual DbSet<Inactivos> Inactivos { get; set; }
        public virtual DbSet<invactivodet> invactivodet { get; set; }
        public virtual DbSet<invactivoenc> invactivoenc { get; set; }
        public virtual DbSet<InvClase> InvClase { get; set; }
        public virtual DbSet<InvClasif> InvClasif { get; set; }
        public virtual DbSet<InvColor> InvColor { get; set; }
        public virtual DbSet<InvCombo> InvCombo { get; set; }
        public virtual DbSet<InvDescDet> InvDescDet { get; set; }
        public virtual DbSet<InvDescEnc> InvDescEnc { get; set; }
        public virtual DbSet<InvDivision> InvDivision { get; set; }
        public virtual DbSet<InvExistencia> InvExistencia { get; set; }
        public virtual DbSet<InvFisDet> InvFisDet { get; set; }
        public virtual DbSet<InvFisEnc> InvFisEnc { get; set; }
        public virtual DbSet<InvGrupo> InvGrupo { get; set; }
        public virtual DbSet<InvListaPrecioDet> InvListaPrecioDet { get; set; }
        public virtual DbSet<InvListaPrecioEnc> InvListaPrecioEnc { get; set; }
        public virtual DbSet<InvMarca> InvMarca { get; set; }
        public virtual DbSet<InvMedida> InvMedida { get; set; }
        public virtual DbSet<InvMetodoPrecio> InvMetodoPrecio { get; set; }
        public virtual DbSet<InvModelo> InvModelo { get; set; }
        public virtual DbSet<InvMovDet> InvMovDet { get; set; }
        public virtual DbSet<InvMovEnc> InvMovEnc { get; set; }
        public virtual DbSet<InvMovEncCentroCosto> InvMovEncCentroCosto { get; set; }
        public virtual DbSet<InvMovEncContable> InvMovEncContable { get; set; }
        public virtual DbSet<InvMovEncImpuesto> InvMovEncImpuesto { get; set; }
        public virtual DbSet<InvNivelPrecio> InvNivelPrecio { get; set; }
        public virtual DbSet<InvPago> InvPago { get; set; }
        public virtual DbSet<InvProducto> InvProducto { get; set; }
        public virtual DbSet<InvProductoPartes> InvProductoPartes { get; set; }
        public virtual DbSet<InvRubro> InvRubro { get; set; }
        public virtual DbSet<InvTalla> InvTalla { get; set; }
        public virtual DbSet<InvTipoDocumento> InvTipoDocumento { get; set; }
        public virtual DbSet<InvTipoMov> InvTipoMov { get; set; }
        public virtual DbSet<LugarTrabajo> LugarTrabajo { get; set; }
        public virtual DbSet<Meses> Meses { get; set; }
        public virtual DbSet<Moneda> Moneda { get; set; }
        public virtual DbSet<MovCaja> MovCaja { get; set; }
        public virtual DbSet<MovimientoBanco> MovimientoBanco { get; set; }
        public virtual DbSet<opcion> opcion { get; set; }
        public virtual DbSet<OrdenarCompra> OrdenarCompra { get; set; }
        public virtual DbSet<Paises> Paises { get; set; }
        public virtual DbSet<Parentezco> Parentezco { get; set; }
        public virtual DbSet<Parientes> Parientes { get; set; }
        public virtual DbSet<PerfilDet> PerfilDet { get; set; }
        public virtual DbSet<PerfilEnc> PerfilEnc { get; set; }
        public virtual DbSet<Periodo> Periodo { get; set; }
        public virtual DbSet<PlanImpuesto> PlanImpuesto { get; set; }
        public virtual DbSet<PlanXDetalleImpuesto> PlanXDetalleImpuesto { get; set; }
        public virtual DbSet<POSTarjeta> POSTarjeta { get; set; }
        public virtual DbSet<PresupConfigDet> PresupConfigDet { get; set; }
        public virtual DbSet<PresupConfigEnc> PresupConfigEnc { get; set; }
        public virtual DbSet<Presupuesto> Presupuesto { get; set; }
        public virtual DbSet<Prioridad> Prioridad { get; set; }
        public virtual DbSet<ProgramaCxP> ProgramaCxP { get; set; }
        public virtual DbSet<Proyecto> Proyecto { get; set; }
        public virtual DbSet<PuntoVenta> PuntoVenta { get; set; }
        public virtual DbSet<Ratio> Ratio { get; set; }
        public virtual DbSet<RetiroEmpleado> RetiroEmpleado { get; set; }
        public virtual DbSet<rptFiltros> rptFiltros { get; set; }
        public virtual DbSet<rrhEmpleado> rrhEmpleado { get; set; }
        public virtual DbSet<SaldoBanco> SaldoBanco { get; set; }
        public virtual DbSet<SaldoCC> SaldoCC { get; set; }
        public virtual DbSet<SaldoConta> SaldoConta { get; set; }
        public virtual DbSet<SolicitudDet> SolicitudDet { get; set; }
        public virtual DbSet<SolicitudEnc> SolicitudEnc { get; set; }
        public virtual DbSet<SolicitudExt> SolicitudExt { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<Sucursal> Sucursal { get; set; }
        public virtual DbSet<Tarjeta> Tarjeta { get; set; }
        public virtual DbSet<TipoAbono> TipoAbono { get; set; }
        public virtual DbSet<TipoActividad> TipoActividad { get; set; }
        public virtual DbSet<TipoActivo> TipoActivo { get; set; }
        public virtual DbSet<TipoAhorro> TipoAhorro { get; set; }
        public virtual DbSet<TipoCredito> TipoCredito { get; set; }
        public virtual DbSet<TipoCuenta> TipoCuenta { get; set; }
        public virtual DbSet<TipoExterno> TipoExterno { get; set; }
        public virtual DbSet<TipoMovBanco> TipoMovBanco { get; set; }
        public virtual DbSet<TipoMovCaja> TipoMovCaja { get; set; }
        public virtual DbSet<TipoOrden> TipoOrden { get; set; }
        public virtual DbSet<TipoPartida> TipoPartida { get; set; }
        public virtual DbSet<TipoUsoOrden> TipoUsoOrden { get; set; }
        public virtual DbSet<Traduccion> Traduccion { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Filtro> Filtro { get; set; }
        public virtual DbSet<FTLocks> FTLocks { get; set; }
        public virtual DbSet<IvarptBase> IvarptBase { get; set; }
        public virtual DbSet<Lista> Lista { get; set; }
        public virtual DbSet<TipoMovIVA> TipoMovIVA { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.ActivoFijoDes)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.Ubicacion)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.NumSerie)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.Fuente)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.ValorOriginal)
                .HasPrecision(15, 2);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.ValorActual)
                .HasPrecision(15, 2);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.VidaUtil)
                .HasPrecision(3, 0);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.Residuo)
                .HasPrecision(5, 2);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.VidaUtilActual)
                .HasPrecision(3, 0);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.Marca)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.Modelo)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .Property(e => e.MotivoRetiro)
                .IsUnicode(false);

            modelBuilder.Entity<ActivoFijo>()
                .HasMany(e => e.Depreciacion)
                .WithRequired(e => e.ActivoFijo)
                .HasForeignKey(e => e.IdActivoFijo);

            modelBuilder.Entity<Asociacion>()
                .Property(e => e.AhorroAnt)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Asociacion>()
                .Property(e => e.AhorroNuevo)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Banco>()
                .Property(e => e.BancoDes)
                .IsUnicode(false);

            modelBuilder.Entity<Banco>()
                .Property(e => e.NombreCorto)
                .IsUnicode(false);

            modelBuilder.Entity<Banco>()
                .Property(e => e.CodCampero)
                .IsUnicode(false);

            modelBuilder.Entity<Banco>()
                .Property(e => e.Formulario)
                .IsUnicode(false);

            modelBuilder.Entity<Banco>()
                .Property(e => e.Formulario2)
                .IsUnicode(false);

            modelBuilder.Entity<Banco>()
                .HasMany(e => e.CuentaBanco)
                .WithRequired(e => e.Banco)
                .HasForeignKey(e => e.IdBanco)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BaseDetalleImpuesto>()
                .Property(e => e.BaseDetalleImpuesto1)
                .IsUnicode(false);

            modelBuilder.Entity<Catalogo>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Catalogo>()
                .Property(e => e.CatalogoDes)
                .IsUnicode(false);

            modelBuilder.Entity<Catalogo>()
                .Property(e => e.CodigoPadre)
                .IsUnicode(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.CatalogoOpcion)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.CatalogoxCatalogoCC)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.CuentaBanco)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.DiarioDet)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.EstResConfig)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.PresupConfigDet)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.Presupuesto)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.SaldoConta)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.TipoAhorro)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCatalogo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Catalogo>()
                .HasMany(e => e.TipoCredito)
                .WithRequired(e => e.Catalogo)
                .HasForeignKey(e => e.IdCtaCredito)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CatalogoCC>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<CatalogoCC>()
                .Property(e => e.CatalogoDes)
                .IsUnicode(false);

            modelBuilder.Entity<CatalogoCC>()
                .Property(e => e.CodigoPadre)
                .IsUnicode(false);

            modelBuilder.Entity<CatalogoCC>()
                .HasMany(e => e.CatalogoxCatalogoCC)
                .WithRequired(e => e.CatalogoCC)
                .HasForeignKey(e => e.IdCatalogoCC)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CatalogoCC>()
                .HasMany(e => e.SaldoCC)
                .WithRequired(e => e.CatalogoCC)
                .HasForeignKey(e => e.IdCatalogoCC)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Categoria>()
                .Property(e => e.CategoriaDes)
                .IsUnicode(false);

            modelBuilder.Entity<Ciudades>()
                .Property(e => e.Codigo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Ciudades>()
                .Property(e => e.CiudadesDes)
                .IsUnicode(false);

            modelBuilder.Entity<ClaseActivo>()
                .Property(e => e.ClaseActivodes)
                .IsUnicode(false);

            modelBuilder.Entity<ClaseActivo>()
                .Property(e => e.Residuo)
                .HasPrecision(5, 2);

            modelBuilder.Entity<ClaseActivo>()
                .Property(e => e.VidaUtil)
                .HasPrecision(3, 0);

            modelBuilder.Entity<ClasifEFE>()
                .Property(e => e.ClasifefeDes)
                .IsUnicode(false);

            modelBuilder.Entity<ClasificaCliente>()
                .Property(e => e.ClasificaDes)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Sueldo)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.DUI)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.NIT)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.NRC)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.eMail)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Sexo)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.PorAhorro)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.NumCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.TipoCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.WebSite)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Giro)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Ganancia)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.ArchivoFoto)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Cargo)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Contacto)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.Asociacion)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.CreditoEnc)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.cxpQuedanEnc)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.DeudaExterna)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.Deudor)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.Inactivos)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.InvMovEnc)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.OrdenarCompra)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.Parientes)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.Proyecto)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdSupervisor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.RetiroEmpleado)
                .WithRequired(e => e.Cliente)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Config>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Config>()
                .Property(e => e.Tipo)
                .IsUnicode(false);

            modelBuilder.Entity<Config>()
                .Property(e => e.Posicion)
                .IsFixedLength();

            modelBuilder.Entity<CorteCaja>()
                .HasMany(e => e.MovCaja)
                .WithRequired(e => e.CorteCaja)
                .HasForeignKey(e => e.IdCorteCaja)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CortePago>()
                .HasMany(e => e.Asociacion)
                .WithRequired(e => e.CortePago)
                .HasForeignKey(e => e.IdCortePago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CortePago>()
                .HasMany(e => e.CreditoEnc)
                .WithRequired(e => e.CortePago)
                .HasForeignKey(e => e.IdCortePago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CortePago>()
                .HasMany(e => e.CredNoEnc)
                .WithRequired(e => e.CortePago)
                .HasForeignKey(e => e.IdCortePago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CortePago>()
                .HasMany(e => e.Inactivos)
                .WithRequired(e => e.CortePago)
                .HasForeignKey(e => e.IdCortePago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CortePago>()
                .HasMany(e => e.SolicitudEnc)
                .WithRequired(e => e.CortePago)
                .HasForeignKey(e => e.IdCortePago)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CreditoDet>()
                .Property(e => e.TasaInteres)
                .HasPrecision(6, 4);

            modelBuilder.Entity<CreditoDet>()
                .Property(e => e.TasaSeguro)
                .HasPrecision(6, 4);

            modelBuilder.Entity<CreditoDet>()
                .Property(e => e.TasaMora)
                .HasPrecision(6, 4);

            modelBuilder.Entity<CreditoEnc>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<CreditoEnc>()
                .Property(e => e.Frecuencia)
                .IsUnicode(false);

            modelBuilder.Entity<CreditoEnc>()
                .Property(e => e.TasaInteres)
                .HasPrecision(6, 4);

            modelBuilder.Entity<CreditoEnc>()
                .Property(e => e.TasaSeguro)
                .HasPrecision(6, 4);

            modelBuilder.Entity<CreditoEnc>()
                .Property(e => e.TasaMora)
                .HasPrecision(6, 4);

            modelBuilder.Entity<CreditoEnc>()
                .HasMany(e => e.CreditoDet)
                .WithRequired(e => e.CreditoEnc)
                .HasForeignKey(e => e.IdCreditoEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CreditoEnc>()
                .HasMany(e => e.SolicitudDet)
                .WithRequired(e => e.CreditoEnc)
                .HasForeignKey(e => e.IdCreditoEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CredNoEnc>()
                .Property(e => e.CodCliente)
                .IsUnicode(false);

            modelBuilder.Entity<CredNoEnc>()
                .Property(e => e.CodCredito)
                .IsUnicode(false);

            modelBuilder.Entity<CriterioCotiza>()
                .Property(e => e.CriterioCotizaDes)
                .IsUnicode(false);

            modelBuilder.Entity<CriterioCotiza>()
                .Property(e => e.Campo)
                .IsUnicode(false);

            modelBuilder.Entity<CriterioCotiza>()
                .Property(e => e.CampoAmostrar)
                .IsUnicode(false);

            modelBuilder.Entity<CuentaBanco>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<CuentaBanco>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<CuentaBanco>()
                .HasMany(e => e.BanConciliacion)
                .WithRequired(e => e.CuentaBanco)
                .HasForeignKey(e => e.IdCuentaBanco)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CuentaBanco>()
                .HasMany(e => e.MovimientoBanco)
                .WithRequired(e => e.CuentaBanco)
                .HasForeignKey(e => e.IdCuentaBanco)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CuentaBanco>()
                .HasMany(e => e.SaldoBanco)
                .WithRequired(e => e.CuentaBanco)
                .HasForeignKey(e => e.IdCuentaBanco);

            modelBuilder.Entity<cxcTipoTransaccion>()
                .Property(e => e.cxcTipoTransaccion1)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTipoTransaccion>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransacciones>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransacciones>()
                .Property(e => e.NumTransaccion)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransacciones>()
                .Property(e => e.NumeroQuedan)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransacciones>()
                .Property(e => e.NumeroChequeTarjeta)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransacciones>()
                .Property(e => e.Origen)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransacciones>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransacciones>()
                .HasMany(e => e.cxcTransaccionesAplicadas)
                .WithRequired(e => e.cxcTransacciones)
                .HasForeignKey(e => e.IdcxcTransaccionAplicado);

            modelBuilder.Entity<cxcTransaccionesAplicadas>()
                .Property(e => e.NumeroAplicar)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransaccionesAplicadas>()
                .Property(e => e.NumeroAplicado)
                .IsUnicode(false);

            modelBuilder.Entity<cxcTransaccionesAplicadas>()
                .Property(e => e.Origen)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesCentroCosto>()
                .Property(e => e.CodigoCuentaCC)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesCentroCosto>()
                .Property(e => e.DescripcionCuentaCC)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesCentroCosto>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesCentroCosto>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesContable>()
                .Property(e => e.CodigoCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesContable>()
                .Property(e => e.DescripcionCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesContable>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<CxcTransaccionesContable>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<cxpQuedanDet>()
                .Property(e => e.TasaIva)
                .HasPrecision(5, 2);

            modelBuilder.Entity<cxpQuedanDet>()
                .Property(e => e.TasaIvaPyme)
                .HasPrecision(5, 2);

            modelBuilder.Entity<cxpQuedanDet>()
                .HasMany(e => e.ProgramaCxP)
                .WithRequired(e => e.cxpQuedanDet)
                .HasForeignKey(e => e.IdCxpQuedanDet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cxpQuedanEnc>()
                .Property(e => e.Numero)
                .HasPrecision(18, 0);

            modelBuilder.Entity<cxpQuedanEnc>()
                .HasMany(e => e.cxpQuedanDet)
                .WithRequired(e => e.cxpQuedanEnc)
                .HasForeignKey(e => e.IdcxpQuedanEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cxpTipoTransaccion>()
                .Property(e => e.cxpTipoTransaccion1)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTipoTransaccion>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransacciones>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransacciones>()
                .Property(e => e.NumTransaccion)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransacciones>()
                .Property(e => e.NumeroQuedan)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransacciones>()
                .Property(e => e.NumeroChequeTarjeta)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransacciones>()
                .Property(e => e.Origen)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransacciones>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransacciones>()
                .HasMany(e => e.cxpTransaccionesAplicadas)
                .WithRequired(e => e.cxpTransacciones)
                .HasForeignKey(e => e.IdcxpTransaccionAplicado);

            modelBuilder.Entity<cxpTransaccionesAplicadas>()
                .Property(e => e.NumeroAplicar)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransaccionesAplicadas>()
                .Property(e => e.NumeroAplicado)
                .IsUnicode(false);

            modelBuilder.Entity<cxpTransaccionesAplicadas>()
                .Property(e => e.Origen)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesCentroCosto>()
                .Property(e => e.CodigoCuentaCC)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesCentroCosto>()
                .Property(e => e.DescripcionCuentaCC)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesCentroCosto>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesCentroCosto>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesContable>()
                .Property(e => e.CodigoCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesContable>()
                .Property(e => e.DescripcionCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesContable>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<CxpTransaccionesContable>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<Departamento>()
                .Property(e => e.DeptoDes)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleImpuesto>()
                .Property(e => e.CodImpuesto)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleImpuesto>()
                .Property(e => e.DesImpuesto)
                .IsUnicode(false);

            modelBuilder.Entity<DetalleImpuesto>()
                .Property(e => e.ValImpuesto)
                .HasPrecision(18, 4);

            modelBuilder.Entity<DetalleImpuesto>()
                .Property(e => e.MontoGraMin)
                .HasPrecision(18, 4);

            modelBuilder.Entity<DetalleImpuesto>()
                .Property(e => e.MontoGraMax)
                .HasPrecision(18, 4);

            modelBuilder.Entity<DetalleImpuesto>()
                .HasMany(e => e.PlanXDetalleImpuesto)
                .WithRequired(e => e.DetalleImpuesto)
                .HasForeignKey(e => e.IdDetalleImpuesto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeudaExterna>()
                .Property(e => e.Referencia)
                .IsUnicode(false);

            modelBuilder.Entity<DeudaExterna>()
                .Property(e => e.Frecuencia)
                .IsUnicode(false);

            modelBuilder.Entity<DeudaExterna>()
                .HasMany(e => e.SolicitudExt)
                .WithRequired(e => e.DeudaExterna)
                .HasForeignKey(e => e.IdDeudaExterna)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Deudor>()
                .HasMany(e => e.SolicitudDet)
                .WithRequired(e => e.Deudor)
                .HasForeignKey(e => e.IdDeudor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Deudor>()
                .HasMany(e => e.SolicitudExt)
                .WithRequired(e => e.Deudor)
                .HasForeignKey(e => e.IdDeudor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DiarioDet>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<DiarioDet>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<DiarioDet>()
                .Property(e => e.TipoExtra)
                .IsUnicode(false);

            modelBuilder.Entity<DiarioEnc>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<DiarioEnc>()
                .Property(e => e.Origen)
                .IsUnicode(false);

            modelBuilder.Entity<DiarioEnc>()
                .HasMany(e => e.cxpQuedanDet)
                .WithRequired(e => e.DiarioEnc)
                .HasForeignKey(e => e.IdDiarioEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DiarioEnc>()
                .HasMany(e => e.DiarioDet)
                .WithRequired(e => e.DiarioEnc)
                .HasForeignKey(e => e.IdDiarioEnc);

            modelBuilder.Entity<Division>()
                .Property(e => e.Codigo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Division>()
                .Property(e => e.Division1)
                .IsUnicode(false);

            modelBuilder.Entity<Documento>()
                .Property(e => e.DocumentoDes)
                .IsUnicode(false);

            modelBuilder.Entity<Documento>()
                .Property(e => e.NomCorto)
                .IsUnicode(false);

            modelBuilder.Entity<Documento>()
                .Property(e => e.Correlativo)
                .IsUnicode(false);

            modelBuilder.Entity<Documento>()
                .Property(e => e.Formulario)
                .IsUnicode(false);

            modelBuilder.Entity<Documento>()
                .Property(e => e.Serie)
                .IsUnicode(false);

            modelBuilder.Entity<Documento>()
                .HasMany(e => e.cxpQuedanDet)
                .WithRequired(e => e.Documento)
                .HasForeignKey(e => e.IdDocumento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Documento>()
                .HasMany(e => e.InvMovEnc)
                .WithRequired(e => e.Documento)
                .HasForeignKey(e => e.IdDocumento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ejercicio>()
                .HasMany(e => e.Periodo)
                .WithRequired(e => e.Ejercicio)
                .HasForeignKey(e => e.IdEjercicio)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.EmpresaDes)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.NomCorto)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Telefono1)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Telefono2)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.CorreoElectronico)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.SitioWeb)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.NIT)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.NRC)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Giro)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.NomRepresenta)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.DUIRepresenta)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.NITRepresenta)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Tel1Representa)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Tel2Representa)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.CorreoRepresenta)
                .IsUnicode(false);

            modelBuilder.Entity<EstadoCivil>()
                .Property(e => e.EstadoCivilDes)
                .IsUnicode(false);

            modelBuilder.Entity<Estados>()
                .Property(e => e.Codigo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Estados>()
                .Property(e => e.EstadoDes)
                .IsUnicode(false);

            modelBuilder.Entity<EstResultado>()
                .Property(e => e.EstResultadoDes)
                .IsUnicode(false);

            modelBuilder.Entity<FormaPago>()
                .Property(e => e.FormaPagoDes)
                .IsUnicode(false);

            modelBuilder.Entity<FTLog>()
                .Property(e => e.accion)
                .IsUnicode(false);

            modelBuilder.Entity<FTLog>()
                .Property(e => e.entidad)
                .IsUnicode(false);

            modelBuilder.Entity<FTLog>()
                .Property(e => e.formulario)
                .IsUnicode(false);

            modelBuilder.Entity<FTLog>()
                .Property(e => e.tabla)
                .IsUnicode(false);

            modelBuilder.Entity<General>()
                .Property(e => e.Codigo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<General>()
                .Property(e => e.General1)
                .IsUnicode(false);

            modelBuilder.Entity<Idioma>()
                .Property(e => e.IdiomaDes)
                .IsUnicode(false);

            modelBuilder.Entity<Idioma>()
                .Property(e => e.IdiomaCol)
                .IsUnicode(false);

            modelBuilder.Entity<invactivoenc>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<InvClase>()
                .Property(e => e.InvClaseDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvClase>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<InvClase>()
                .HasMany(e => e.InvProducto)
                .WithRequired(e => e.InvClase)
                .HasForeignKey(e => e.IdInvClase)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvClasif>()
                .Property(e => e.InvClasifDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvClasif>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<InvClasif>()
                .HasMany(e => e.InvProducto)
                .WithRequired(e => e.InvClasif)
                .HasForeignKey(e => e.IdInvClasif)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvColor>()
                .Property(e => e.InvColorDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvColor>()
                .HasMany(e => e.InvExistencia)
                .WithRequired(e => e.InvColor)
                .HasForeignKey(e => e.IdInvColor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvDescDet>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 2);

            modelBuilder.Entity<InvDescEnc>()
                .Property(e => e.Numero)
                .HasPrecision(18, 0);

            modelBuilder.Entity<InvDescEnc>()
                .HasMany(e => e.InvDescDet)
                .WithRequired(e => e.InvDescEnc)
                .HasForeignKey(e => e.IdInvDescEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvDivision>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<InvDivision>()
                .Property(e => e.InvDivisionDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvExistencia>()
                .Property(e => e.Costo)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvExistencia>()
                .Property(e => e.Precio)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvExistencia>()
                .Property(e => e.Existencia)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvExistencia>()
                .Property(e => e.CodigoBarra)
                .IsUnicode(false);

            modelBuilder.Entity<InvExistencia>()
                .Property(e => e.OldCode)
                .IsUnicode(false);

            modelBuilder.Entity<InvExistencia>()
                .Property(e => e.Info)
                .IsUnicode(false);

            modelBuilder.Entity<InvExistencia>()
                .HasMany(e => e.InvDescDet)
                .WithRequired(e => e.InvExistencia)
                .HasForeignKey(e => e.IdInvExistencia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvExistencia>()
                .HasMany(e => e.InvFisDet)
                .WithRequired(e => e.InvExistencia)
                .HasForeignKey(e => e.IdInvExistencia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvFisEnc>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<InvFisEnc>()
                .HasMany(e => e.InvFisDet)
                .WithRequired(e => e.InvFisEnc)
                .HasForeignKey(e => e.IdInvFisEnc);

            modelBuilder.Entity<InvGrupo>()
                .Property(e => e.InvGrupoDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvGrupo>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<InvGrupo>()
                .HasMany(e => e.InvProducto)
                .WithRequired(e => e.InvGrupo)
                .HasForeignKey(e => e.IdInvGrupo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvListaPrecioDet>()
                .Property(e => e.Precio)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvListaPrecioDet>()
                .Property(e => e.CantidadInicial)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvListaPrecioDet>()
                .Property(e => e.CantidadFinal)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvListaPrecioEnc>()
                .HasMany(e => e.InvListaPrecioDet)
                .WithRequired(e => e.InvListaPrecioEnc)
                .HasForeignKey(e => e.IdInvListaPrecioEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvMarca>()
                .Property(e => e.InvMarcaDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvMarca>()
                .HasMany(e => e.InvProducto)
                .WithRequired(e => e.InvMarca)
                .HasForeignKey(e => e.IdInvMarca)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvMedida>()
                .Property(e => e.InvMedidaDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvMedida>()
                .HasMany(e => e.InvProducto)
                .WithOptional(e => e.InvMedida)
                .HasForeignKey(e => e.IdInvMedida);

            modelBuilder.Entity<InvMetodoPrecio>()
                .Property(e => e.InvMetodoPrecioDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvModelo>()
                .Property(e => e.InvModeloDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 4);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.Costo)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.Precio)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.TasaDescuento)
                .HasPrecision(4, 2);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.TasaIva)
                .HasPrecision(4, 2);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.TasaCosto)
                .HasPrecision(4, 2);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.TasaPrecio)
                .HasPrecision(4, 2);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.CantBodega)
                .HasPrecision(12, 2);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.PrecioUnitario)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.PrecioNeto)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.NumeroParte)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.NumeroSerie)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovDet>()
                .Property(e => e.ExistAntParte)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.Quedan)
                .HasPrecision(18, 0);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.Enviara)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.OrdenProduccion)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.Atencion)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.NumeroChequeTarjeta)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.Origen)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEnc>()
                .Property(e => e.Serie)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEnc>()
                .HasMany(e => e.InvMovDet)
                .WithRequired(e => e.InvMovEnc)
                .HasForeignKey(e => e.IdInvMovEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvMovEncCentroCosto>()
                .Property(e => e.CodigoCuentaCC)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncCentroCosto>()
                .Property(e => e.DescripcionCuentaCC)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncCentroCosto>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncCentroCosto>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncContable>()
                .Property(e => e.CodigoCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncContable>()
                .Property(e => e.DescripcionCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncContable>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncContable>()
                .Property(e => e.Documento)
                .IsUnicode(false);

            modelBuilder.Entity<InvMovEncImpuesto>()
                .Property(e => e.MontoImpuesto)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovEncImpuesto>()
                .Property(e => e.MontoVenta)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovEncImpuesto>()
                .Property(e => e.ValImpuesto)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovEncImpuesto>()
                .Property(e => e.MontoGraMin)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvMovEncImpuesto>()
                .Property(e => e.MontoGraMax)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvNivelPrecio>()
                .Property(e => e.InvNivelPrecioDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvPago>()
                .Property(e => e.Frecuencia)
                .IsUnicode(false);

            modelBuilder.Entity<InvPago>()
                .Property(e => e.NumTarjeta)
                .IsUnicode(false);

            modelBuilder.Entity<InvPago>()
                .Property(e => e.NumCheque)
                .IsUnicode(false);

            modelBuilder.Entity<InvProducto>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<InvProducto>()
                .Property(e => e.InvProductoDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvProducto>()
                .Property(e => e.NomComercial)
                .IsUnicode(false);

            modelBuilder.Entity<InvProducto>()
                .Property(e => e.ConocidoPor)
                .IsUnicode(false);

            modelBuilder.Entity<InvProducto>()
                .Property(e => e.Costo)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvProducto>()
                .Property(e => e.Existencia)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvProducto>()
                .HasMany(e => e.InvListaPrecioEnc)
                .WithRequired(e => e.InvProducto)
                .HasForeignKey(e => e.IdInvProducto);

            modelBuilder.Entity<InvProducto>()
                .HasMany(e => e.InvMovDet)
                .WithRequired(e => e.InvProducto)
                .HasForeignKey(e => e.IdInvProducto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvProducto>()
                .HasMany(e => e.InvProductoPartes)
                .WithRequired(e => e.InvProducto)
                .HasForeignKey(e => e.IdInvProducto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvProductoPartes>()
                .Property(e => e.Modelo)
                .IsUnicode(false);

            modelBuilder.Entity<InvProductoPartes>()
                .Property(e => e.NumeroParte)
                .IsUnicode(false);

            modelBuilder.Entity<InvProductoPartes>()
                .Property(e => e.NumeroSerie)
                .IsUnicode(false);

            modelBuilder.Entity<InvProductoPartes>()
                .Property(e => e.CodigoBarra1)
                .IsUnicode(false);

            modelBuilder.Entity<InvProductoPartes>()
                .Property(e => e.CodigoBarra2)
                .IsUnicode(false);

            modelBuilder.Entity<InvProductoPartes>()
                .Property(e => e.Existencia)
                .HasPrecision(18, 4);

            modelBuilder.Entity<InvRubro>()
                .Property(e => e.InvRubroDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvRubro>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<InvRubro>()
                .HasMany(e => e.InvProducto)
                .WithRequired(e => e.InvRubro)
                .HasForeignKey(e => e.IdInvModelo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvTalla>()
                .Property(e => e.InvTallaDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvTalla>()
                .HasMany(e => e.InvExistencia)
                .WithRequired(e => e.InvTalla)
                .HasForeignKey(e => e.IdInvTalla)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvTipoDocumento>()
                .Property(e => e.InvTipoDocumentoDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvTipoMov>()
                .Property(e => e.InvTipoMovDes)
                .IsUnicode(false);

            modelBuilder.Entity<InvTipoMov>()
                .HasMany(e => e.InvMovEnc)
                .WithRequired(e => e.InvTipoMov)
                .HasForeignKey(e => e.IdInvTipoMov)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LugarTrabajo>()
                .Property(e => e.LugarTrabajoDes)
                .IsUnicode(false);

            modelBuilder.Entity<LugarTrabajo>()
                .HasMany(e => e.Deudor)
                .WithRequired(e => e.LugarTrabajo)
                .HasForeignKey(e => e.IdLugarTrabajo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Meses>()
                .Property(e => e.Mes)
                .IsUnicode(false);

            modelBuilder.Entity<Meses>()
                .HasMany(e => e.Periodo)
                .WithRequired(e => e.Meses)
                .HasForeignKey(e => e.IdMes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Moneda>()
                .Property(e => e.Moneda1)
                .IsUnicode(false);

            modelBuilder.Entity<Moneda>()
                .Property(e => e.NomSingular)
                .IsUnicode(false);

            modelBuilder.Entity<Moneda>()
                .Property(e => e.NomPlural)
                .IsUnicode(false);

            modelBuilder.Entity<Moneda>()
                .Property(e => e.Simbolo)
                .IsUnicode(false);

            modelBuilder.Entity<Moneda>()
                .Property(e => e.EquivalenciaDolar)
                .HasPrecision(18, 5);

            modelBuilder.Entity<Moneda>()
                .Property(e => e.EquivalenciaEuro)
                .HasPrecision(18, 5);

            modelBuilder.Entity<MovCaja>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<MovCaja>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<MovimientoBanco>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<MovimientoBanco>()
                .Property(e => e.Paguese)
                .IsUnicode(false);

            modelBuilder.Entity<MovimientoBanco>()
                .Property(e => e.NDocumentoBanco)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.opciontipo)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.opciondes)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.programa)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.vbindex)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.Posicion)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.Ico)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.filtro)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .Property(e => e.SeleccionSQL)
                .IsUnicode(false);

            modelBuilder.Entity<opcion>()
                .HasMany(e => e.CatalogoOpcion)
                .WithRequired(e => e.opcion)
                .HasForeignKey(e => e.IdOpcion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paises>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Paises>()
                .Property(e => e.PaisDes)
                .IsUnicode(false);

            modelBuilder.Entity<Paises>()
                .Property(e => e.CodigoArea)
                .IsUnicode(false);

            modelBuilder.Entity<Parentezco>()
                .Property(e => e.ParentescoDes)
                .IsUnicode(false);

            modelBuilder.Entity<Parentezco>()
                .HasMany(e => e.Parientes)
                .WithRequired(e => e.Parentezco)
                .HasForeignKey(e => e.IdParentezco)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Parientes>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Parientes>()
                .Property(e => e.Porcentaje)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Parientes>()
                .Property(e => e.Sexo)
                .IsUnicode(false);

            modelBuilder.Entity<PerfilEnc>()
                .Property(e => e.Codigo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PerfilEnc>()
                .Property(e => e.PerfilDes)
                .IsUnicode(false);

            modelBuilder.Entity<PerfilEnc>()
                .HasMany(e => e.PerfilDet)
                .WithRequired(e => e.PerfilEnc)
                .HasForeignKey(e => e.idPerfilEnc);

            modelBuilder.Entity<PerfilEnc>()
                .HasMany(e => e.Usuario)
                .WithRequired(e => e.PerfilEnc)
                .HasForeignKey(e => e.idperfilenc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.BanConciliacion)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.CreditoEnc)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.cxpQuedanEnc)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.DiarioEnc)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.InvFisEnc)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.InvMovEnc)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.MovimientoBanco)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.Presupuesto)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.SaldoBanco)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.SaldoCC)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodo>()
                .HasMany(e => e.SaldoConta)
                .WithRequired(e => e.Periodo)
                .HasForeignKey(e => e.IdPeriodo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PlanImpuesto>()
                .Property(e => e.CodPlanImpuesto)
                .IsUnicode(false);

            modelBuilder.Entity<PlanImpuesto>()
                .Property(e => e.DesPlanImpuesto)
                .IsUnicode(false);

            modelBuilder.Entity<PlanImpuesto>()
                .HasMany(e => e.Cliente)
                .WithRequired(e => e.PlanImpuesto)
                .HasForeignKey(e => e.IdPlanImpuesto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PlanImpuesto>()
                .HasMany(e => e.PlanXDetalleImpuesto)
                .WithRequired(e => e.PlanImpuesto)
                .HasForeignKey(e => e.IdPlanImpuesto)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<POSTarjeta>()
                .Property(e => e.POSTarjetaDes)
                .IsUnicode(false);

            modelBuilder.Entity<PresupConfigEnc>()
                .Property(e => e.Concepto)
                .IsUnicode(false);

            modelBuilder.Entity<PresupConfigEnc>()
                .HasMany(e => e.PresupConfigDet)
                .WithRequired(e => e.PresupConfigEnc)
                .HasForeignKey(e => e.IdPresupConfigEnc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prioridad>()
                .Property(e => e.PrioridadDes)
                .IsUnicode(false);

            modelBuilder.Entity<Proyecto>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Proyecto>()
                .Property(e => e.ProyectoDes)
                .IsUnicode(false);

            modelBuilder.Entity<Proyecto>()
                .Property(e => e.CodGeneral)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Proyecto>()
                .Property(e => e.CodAnio)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Proyecto>()
                .Property(e => e.CodDivision)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Proyecto>()
                .Property(e => e.CodCliente)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Proyecto>()
                .Property(e => e.Correlativo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PuntoVenta>()
                .Property(e => e.PuntoVentaDes)
                .IsUnicode(false);

            modelBuilder.Entity<PuntoVenta>()
                .HasMany(e => e.CorteCaja)
                .WithRequired(e => e.PuntoVenta)
                .HasForeignKey(e => e.IdPuntoVenta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PuntoVenta>()
                .HasMany(e => e.InvDescEnc)
                .WithRequired(e => e.PuntoVenta)
                .HasForeignKey(e => e.IdPuntoVenta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ratio>()
                .Property(e => e.Titulo)
                .IsUnicode(false);

            modelBuilder.Entity<RetiroEmpleado>()
                .Property(e => e.Referencia)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.NombreCampo)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.CampoDescriptivo)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.TituloCampo)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.TipodeDato)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.SqlBoton)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.TitulosBusqueda)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.AnchosBusqueda)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.CampoAMostrar)
                .IsUnicode(false);

            modelBuilder.Entity<rptFiltros>()
                .Property(e => e.SqlTableAlias)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.PrimerNombre)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.SegundoNombre)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.Apellidos)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.ApellidoCasada)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.Telefono1)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.Telefono2)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.CorreoElectronico)
                .IsUnicode(false);

            modelBuilder.Entity<rrhEmpleado>()
                .Property(e => e.IdDepartamento)
                .IsFixedLength();

            modelBuilder.Entity<SolicitudDet>()
                .Property(e => e.Frecuencia)
                .IsUnicode(false);

            modelBuilder.Entity<SolicitudEnc>()
                .Property(e => e.Frecuencia)
                .IsUnicode(false);

            modelBuilder.Entity<SolicitudEnc>()
                .Property(e => e.TasaInteres)
                .HasPrecision(6, 4);

            modelBuilder.Entity<SolicitudEnc>()
                .Property(e => e.TasaSeguro)
                .HasPrecision(6, 4);

            modelBuilder.Entity<SolicitudEnc>()
                .Property(e => e.TasaMora)
                .HasPrecision(6, 4);

            modelBuilder.Entity<SolicitudExt>()
                .Property(e => e.Frecuencia)
                .IsUnicode(false);

            modelBuilder.Entity<Status>()
                .Property(e => e.StatusDes)
                .IsUnicode(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.CreditoEnc)
                .WithRequired(e => e.Status)
                .HasForeignKey(e => e.IdStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sucursal>()
                .Property(e => e.SucursalDes)
                .IsUnicode(false);

            modelBuilder.Entity<Sucursal>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Sucursal>()
                .Property(e => e.Telefono1)
                .IsUnicode(false);

            modelBuilder.Entity<Sucursal>()
                .Property(e => e.Telefono2)
                .IsUnicode(false);

            modelBuilder.Entity<Sucursal>()
                .HasMany(e => e.CreditoEnc)
                .WithRequired(e => e.Sucursal)
                .HasForeignKey(e => e.IdSucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sucursal>()
                .HasMany(e => e.cxpQuedanEnc)
                .WithRequired(e => e.Sucursal)
                .HasForeignKey(e => e.IdSucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sucursal>()
                .HasMany(e => e.InvExistencia)
                .WithRequired(e => e.Sucursal)
                .HasForeignKey(e => e.IdSucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sucursal>()
                .HasMany(e => e.InvFisEnc)
                .WithRequired(e => e.Sucursal)
                .HasForeignKey(e => e.IdSucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sucursal>()
                .HasMany(e => e.InvMovEnc)
                .WithRequired(e => e.Sucursal)
                .HasForeignKey(e => e.IdSucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sucursal>()
                .HasMany(e => e.PuntoVenta)
                .WithRequired(e => e.Sucursal)
                .HasForeignKey(e => e.IdSucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sucursal>()
                .HasMany(e => e.SolicitudEnc)
                .WithRequired(e => e.Sucursal)
                .HasForeignKey(e => e.IdSucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tarjeta>()
                .Property(e => e.TarjetaDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoAbono>()
                .Property(e => e.TipoAbonoDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoAbono>()
                .HasMany(e => e.CreditoDet)
                .WithRequired(e => e.TipoAbono)
                .HasForeignKey(e => e.IdTipoAbono)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoActividad>()
                .Property(e => e.TipoActividad1)
                .IsUnicode(false);

            modelBuilder.Entity<TipoActivo>()
                .Property(e => e.TipoActivodes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoAhorro>()
                .Property(e => e.TipoAhorroDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoCredito>()
                .Property(e => e.TipoCreditoDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoCredito>()
                .Property(e => e.TasaInteres)
                .HasPrecision(6, 4);

            modelBuilder.Entity<TipoCredito>()
                .Property(e => e.TasaSeguro)
                .HasPrecision(6, 4);

            modelBuilder.Entity<TipoCredito>()
                .Property(e => e.TasaMora)
                .HasPrecision(6, 4);

            modelBuilder.Entity<TipoCredito>()
                .HasMany(e => e.CreditoEnc)
                .WithRequired(e => e.TipoCredito)
                .HasForeignKey(e => e.IdTipoCredito)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoCredito>()
                .HasMany(e => e.InvPago)
                .WithRequired(e => e.TipoCredito)
                .HasForeignKey(e => e.IdTipoCredito)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoCredito>()
                .HasMany(e => e.SolicitudEnc)
                .WithRequired(e => e.TipoCredito)
                .HasForeignKey(e => e.IdTipoCredito)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoCuenta>()
                .Property(e => e.TipoCuentaDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoCuenta>()
                .Property(e => e.NomCorto)
                .IsUnicode(false);

            modelBuilder.Entity<TipoCuenta>()
                .HasMany(e => e.Catalogo)
                .WithRequired(e => e.TipoCuenta)
                .HasForeignKey(e => e.IdTipoCuenta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoExterno>()
                .Property(e => e.TipoExternoDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoExterno>()
                .HasMany(e => e.DeudaExterna)
                .WithRequired(e => e.TipoExterno)
                .HasForeignKey(e => e.IdTipoExterno)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoMovBanco>()
                .Property(e => e.TipoMovBancoDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoMovBanco>()
                .HasMany(e => e.MovimientoBanco)
                .WithRequired(e => e.TipoMovBanco)
                .HasForeignKey(e => e.IdTipoMovBanco)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoMovCaja>()
                .Property(e => e.TipoMovCajaDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoMovCaja>()
                .HasMany(e => e.MovCaja)
                .WithRequired(e => e.TipoMovCaja)
                .HasForeignKey(e => e.IdTipoMovCaja)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoOrden>()
                .Property(e => e.Monto)
                .HasPrecision(5, 2);

            modelBuilder.Entity<TipoPartida>()
                .Property(e => e.TipoPartidaDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoPartida>()
                .HasMany(e => e.DiarioEnc)
                .WithRequired(e => e.TipoPartida)
                .HasForeignKey(e => e.IdTipoPartida)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoPartida>()
                .HasMany(e => e.TipoMovBanco)
                .WithRequired(e => e.TipoPartida)
                .HasForeignKey(e => e.IdTipoPartida)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoUsoOrden>()
                .Property(e => e.TipoUsoOrdenDes)
                .IsUnicode(false);

            modelBuilder.Entity<TipoUsoOrden>()
                .Property(e => e.TasaDescuento)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Traduccion>()
                .Property(e => e.Formulario)
                .IsUnicode(false);

            modelBuilder.Entity<Traduccion>()
                .Property(e => e.Control)
                .IsUnicode(false);

            modelBuilder.Entity<Traduccion>()
                .Property(e => e.Espanol)
                .IsUnicode(false);

            modelBuilder.Entity<Traduccion>()
                .Property(e => e.Ingles)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.NombreCompleto)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.clave)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.SubUsuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.SubIndex)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.KeyVenta)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.AccesoUsuario)
                .WithRequired(e => e.Usuario)
                .HasForeignKey(e => e.IdUsuario);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.InvMovEnc)
                .WithRequired(e => e.Usuario)
                .HasForeignKey(e => e.IdUsuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FTLocks>()
                .Property(e => e.Tabla)
                .IsUnicode(false);

            modelBuilder.Entity<FTLocks>()
                .Property(e => e.Opcion)
                .IsUnicode(false);

            modelBuilder.Entity<FTLocks>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.opciontipo)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.opciondes)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.programa)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.vbindex)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.Posicion)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.Ico)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.filtro)
                .IsUnicode(false);

            modelBuilder.Entity<Lista>()
                .Property(e => e.SeleccionSql)
                .IsUnicode(false);

            modelBuilder.Entity<TipoMovIVA>()
                .Property(e => e.TipoMovIVADes)
                .IsUnicode(false);
        }
    }
}
