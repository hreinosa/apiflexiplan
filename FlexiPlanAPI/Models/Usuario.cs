namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Usuario")]
    public partial class Usuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuario()
        {
            AccesoUsuario = new HashSet<AccesoUsuario>();
            InvMovEnc = new HashSet<InvMovEnc>();
        }

        public long id { get; set; }

        public long idperfilenc { get; set; }

        [Required]
        [StringLength(20)]
        public string Codigo { get; set; }

        [StringLength(50)]
        public string NombreCompleto { get; set; }

        [Required]
        [StringLength(20)]
        public string clave { get; set; }

        [StringLength(20)]
        public string SubUsuario { get; set; }

        [StringLength(2)]
        public string SubIndex { get; set; }

        public bool activo { get; set; }

        public bool Logged { get; set; }

        public byte Sessions { get; set; }

        public bool Autorizado { get; set; }

        [StringLength(20)]
        public string KeyVenta { get; set; }

        public bool SobreFac { get; set; }

        public bool EsComprador { get; set; }

        public bool EsVendedor { get; set; }

        public bool EsCajero { get; set; }

        public bool AutorizaCompras { get; set; }

        [StringLength(250)]
        public string email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccesoUsuario> AccesoUsuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvMovEnc> InvMovEnc { get; set; }

        public virtual PerfilEnc PerfilEnc { get; set; }
    }
}
