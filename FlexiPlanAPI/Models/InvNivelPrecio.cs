namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvNivelPrecio")]
    public partial class InvNivelPrecio
    {
        public long Id { get; set; }

        [Required]
        [StringLength(35)]
        public string InvNivelPrecioDes { get; set; }

        public long IdEmpresa { get; set; }
    }
}
