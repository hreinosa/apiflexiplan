namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvTalla")]
    public partial class InvTalla
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvTalla()
        {
            InvExistencia = new HashSet<InvExistencia>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(15)]
        public string InvTallaDes { get; set; }

        public bool Multitalla { get; set; }

        public int Orden { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvExistencia> InvExistencia { get; set; }
    }
}
