namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvClase")]
    public partial class InvClase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvClase()
        {
            InvProducto = new HashSet<InvProducto>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(40)]
        public string InvClaseDes { get; set; }

        [StringLength(4)]
        public string Codigo { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvProducto> InvProducto { get; set; }
    }
}
