namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoUsoOrden")]
    public partial class TipoUsoOrden
    {
        public long Id { get; set; }

        [StringLength(30)]
        public string TipoUsoOrdenDes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaDescuento { get; set; }
    }
}
