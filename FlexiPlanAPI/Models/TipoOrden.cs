namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoOrden")]
    public partial class TipoOrden
    {
        public long Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Monto { get; set; }
    }
}
