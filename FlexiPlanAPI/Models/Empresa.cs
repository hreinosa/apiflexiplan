namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Empresa")]
    public partial class Empresa
    {
        public long Id { get; set; }

        public long IdCategoria { get; set; }

        [Required]
        [StringLength(60)]
        public string EmpresaDes { get; set; }

        [StringLength(30)]
        public string NomCorto { get; set; }

        [StringLength(100)]
        public string Direccion { get; set; }

        [StringLength(15)]
        public string Telefono1 { get; set; }

        [StringLength(15)]
        public string Telefono2 { get; set; }

        [StringLength(15)]
        public string Fax { get; set; }

        [StringLength(65)]
        public string CorreoElectronico { get; set; }

        [StringLength(65)]
        public string SitioWeb { get; set; }

        [StringLength(20)]
        public string NIT { get; set; }

        [StringLength(15)]
        public string NRC { get; set; }

        [StringLength(100)]
        public string Giro { get; set; }

        [StringLength(75)]
        public string NomRepresenta { get; set; }

        [StringLength(15)]
        public string DUIRepresenta { get; set; }

        [StringLength(20)]
        public string NITRepresenta { get; set; }

        [StringLength(15)]
        public string Tel1Representa { get; set; }

        [StringLength(15)]
        public string Tel2Representa { get; set; }

        [StringLength(65)]
        public string CorreoRepresenta { get; set; }
    }
}
