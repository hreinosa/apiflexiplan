namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvMovEnc")]
    public partial class InvMovEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvMovEnc()
        {
            InvMovDet = new HashSet<InvMovDet>();
        }

        public long Id { get; set; }

        public long IdPadre { get; set; }

        public long IdDocumento { get; set; }

        public long IdInvTipoMov { get; set; }

        public long IdCliente { get; set; }

        public long IdProyecto { get; set; }

        public long IdSucursal { get; set; }

        public long IdPeriodo { get; set; }

        public long IdUsuario { get; set; }

        public DateTime Fecha { get; set; }

        [Required]
        [StringLength(20)]
        public string Numero { get; set; }

        public bool Credito { get; set; }

        public byte Plazo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Quedan { get; set; }

        public DateTime? FechaPago { get; set; }

        public bool Nulo { get; set; }

        public bool Impreso { get; set; }

        public bool Cerrado { get; set; }

        public long? IdSolicita { get; set; }

        public long? IdAutoriza { get; set; }

        public long? IdPrioridad { get; set; }

        public byte Entrega { get; set; }

        public byte Validez { get; set; }

        public byte Garantia { get; set; }

        public bool EsRequisicion { get; set; }

        [StringLength(100)]
        public string Enviara { get; set; }

        public long IdCxpQuedanEnc { get; set; }

        public long NumeroCR { get; set; }

        public DateTime? FechaCR { get; set; }

        [StringLength(255)]
        public string OrdenProduccion { get; set; }

        public long IdCxcQuedanEnc { get; set; }

        public long IdDiarioEnc { get; set; }

        public bool Banco { get; set; }

        public long IdCuentaBanco { get; set; }

        public DateTime? FechaSolicitud { get; set; }

        public DateTime? FechaEntrega { get; set; }

        public DateTime? HoraSolicitud { get; set; }

        public DateTime? HoraEntrega { get; set; }

        [StringLength(60)]
        public string Atencion { get; set; }

        public long IdPlanImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Miscelaneos { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotalImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotalDocumento { get; set; }

        public long NumeroCP { get; set; }

        public DateTime? FechaCP { get; set; }

        public bool Contabilidad { get; set; }

        public long IdEmpresa { get; set; }

        public bool AnticipoOperacion { get; set; }

        public short FormaPago { get; set; }

        public long IdTipoMovBanco { get; set; }

        public long IdMovimientoBanco { get; set; }

        public long NumeroMovBanco { get; set; }

        public DateTime? FechaMovBanco { get; set; }

        public long IdTarjeta { get; set; }

        [StringLength(20)]
        public string NumeroChequeTarjeta { get; set; }

        public long IdSucursalDestino { get; set; }

        public long IdInvTipoDocumento { get; set; }

        public short Signo { get; set; }

        [StringLength(30)]
        public string Origen { get; set; }

        public long IdInvOrdenCompra { get; set; }

        public long IdActivoFijo { get; set; }

        public bool Seleccion { get; set; }

        [StringLength(10)]
        public string Serie { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual Documento Documento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvMovDet> InvMovDet { get; set; }

        public virtual InvTipoMov InvTipoMov { get; set; }

        public virtual Periodo Periodo { get; set; }

        public virtual Sucursal Sucursal { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
