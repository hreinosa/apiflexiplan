namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ratio")]
    public partial class Ratio
    {
        public long Id { get; set; }

        public long IdCta1 { get; set; }

        public long IdCta2 { get; set; }

        public short Orden { get; set; }

        [Required]
        [StringLength(60)]
        public string Titulo { get; set; }

        public long IdEmpresa { get; set; }
    }
}
