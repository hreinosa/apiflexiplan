namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvGrupo")]
    public partial class InvGrupo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvGrupo()
        {
            InvProducto = new HashSet<InvProducto>();
        }

        public long ID { get; set; }

        [Required]
        [StringLength(30)]
        public string InvGrupoDes { get; set; }

        [StringLength(4)]
        public string Codigo { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvProducto> InvProducto { get; set; }
    }
}
