namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvRubro")]
    public partial class InvRubro
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvRubro()
        {
            InvProducto = new HashSet<InvProducto>();
        }

        public long Id { get; set; }

        public long IdCatalogo { get; set; }

        [Required]
        [StringLength(30)]
        public string InvRubroDes { get; set; }

        [StringLength(2)]
        public string Codigo { get; set; }

        public bool Seleccion { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvProducto> InvProducto { get; set; }
    }
}
