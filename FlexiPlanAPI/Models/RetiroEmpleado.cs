namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RetiroEmpleado")]
    public partial class RetiroEmpleado
    {
        public long Id { get; set; }

        public long IdCliente { get; set; }

        public long IdPeriodo { get; set; }

        public long? IdMovimientoBanco { get; set; }

        [StringLength(10)]
        public string Referencia { get; set; }

        public DateTime Fecha { get; set; }

        public bool RetEmp { get; set; }

        public bool RetAsoc { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
