namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Categoria")]
    public partial class Categoria
    {
        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        public string CategoriaDes { get; set; }

        public bool Pequenia { get; set; }

        public bool Mediana { get; set; }

        public bool Grande { get; set; }
    }
}
