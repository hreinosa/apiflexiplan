namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvFisDet")]
    public partial class InvFisDet
    {
        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdInvFisEnc { get; set; }

        public long IdInvProducto { get; set; }

        public long IdInvExistencia { get; set; }

        public long IdSucursal { get; set; }

        public long IdUsuario { get; set; }

        public int Secuencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CantidadSistema { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CantidadFisica { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CostoAnterior { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CostoActual { get; set; }

        public long IdInvListaPrecioDet { get; set; }

        public long IdInvNivelPrecio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PrecioUnitarioAnterior { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PrecioUnitarioActual { get; set; }

        public bool IncluyeImpuesto { get; set; }

        public long IdDetalleImpuesto { get; set; }

        public long IdInvClase { get; set; }

        public virtual InvExistencia InvExistencia { get; set; }

        public virtual InvFisEnc InvFisEnc { get; set; }
    }
}
