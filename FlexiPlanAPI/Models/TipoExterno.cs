namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoExterno")]
    public partial class TipoExterno
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoExterno()
        {
            DeudaExterna = new HashSet<DeudaExterna>();
        }

        public long Id { get; set; }

        public short CodCampero { get; set; }

        [Required]
        [StringLength(100)]
        public string TipoExternoDes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeudaExterna> DeudaExterna { get; set; }
    }
}
