namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cxpQuedanDet")]
    public partial class cxpQuedanDet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cxpQuedanDet()
        {
            ProgramaCxP = new HashSet<ProgramaCxP>();
        }

        public long Id { get; set; }

        public long IdcxpQuedanEnc { get; set; }

        public long IdInvMovEnc { get; set; }

        public long IdDiarioEnc { get; set; }

        public long IdDocumento { get; set; }

        public long IdCatalogo { get; set; }

        public long IdPadre { get; set; }

        public long Numero { get; set; }

        public DateTime Fecha { get; set; }

        public short Secuencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorExento { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorGravado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaIva { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaIvaPyme { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CtaPorPagar { get; set; }

        public long NumeroCR { get; set; }

        public DateTime? FechaCR { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotalImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotalDocumento { get; set; }

        public long NumeroCP { get; set; }

        public DateTime? FechaCP { get; set; }

        public bool Contabilidad { get; set; }

        public long IdEmpresa { get; set; }

        public virtual cxpQuedanEnc cxpQuedanEnc { get; set; }

        public virtual DiarioEnc DiarioEnc { get; set; }

        public virtual Documento Documento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgramaCxP> ProgramaCxP { get; set; }
    }
}
