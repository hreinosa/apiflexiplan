namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Paises
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [StringLength(3)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(60)]
        public string PaisDes { get; set; }

        [StringLength(5)]
        public string CodigoArea { get; set; }

        public short CodigoISO { get; set; }
    }
}
