namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CatalogoOpcion")]
    public partial class CatalogoOpcion
    {
        public long Id { get; set; }

        public long IdCatalogo { get; set; }

        public long IdOpcion { get; set; }

        public long IdEmpresa { get; set; }

        public virtual Catalogo Catalogo { get; set; }

        public virtual opcion opcion { get; set; }
    }
}
