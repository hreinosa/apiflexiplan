namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoAbono")]
    public partial class TipoAbono
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoAbono()
        {
            CreditoDet = new HashSet<CreditoDet>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        public string TipoAbonoDes { get; set; }

        public bool Efectivo { get; set; }

        public bool Cheque { get; set; }

        public bool Tarjeta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditoDet> CreditoDet { get; set; }
    }
}
