namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PlanXDetalleImpuesto")]
    public partial class PlanXDetalleImpuesto
    {
        public long Id { get; set; }

        public long IdPlanImpuesto { get; set; }

        public long IdDetalleImpuesto { get; set; }

        public long IdEmpresa { get; set; }

        public virtual DetalleImpuesto DetalleImpuesto { get; set; }

        public virtual PlanImpuesto PlanImpuesto { get; set; }
    }
}
