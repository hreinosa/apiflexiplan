namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Depreciacion")]
    public partial class Depreciacion
    {
        public long Id { get; set; }

        public long IdActivoFijo { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime Mes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal valoranterior { get; set; }

        [Column(TypeName = "numeric")]
        public decimal valoractual { get; set; }

        public int secuencia { get; set; }

        public long IdEmpresa { get; set; }

        public long IdDiarioEnc { get; set; }

        public bool Contabilizado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal DepreMensual { get; set; }

        public long IdPeriodo { get; set; }

        public virtual ActivoFijo ActivoFijo { get; set; }
    }
}
