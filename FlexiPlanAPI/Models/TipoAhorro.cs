namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoAhorro")]
    public partial class TipoAhorro
    {
        public long Id { get; set; }

        public long IdCatalogo { get; set; }

        [StringLength(40)]
        public string TipoAhorroDes { get; set; }

        public bool Ordinario { get; set; }

        public bool Extra { get; set; }

        public bool Patronal { get; set; }

        public bool Dividendo { get; set; }

        public virtual Catalogo Catalogo { get; set; }
    }
}
