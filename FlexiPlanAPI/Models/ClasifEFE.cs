namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ClasifEFE")]
    public partial class ClasifEFE
    {
        public long Id { get; set; }

        [Required]
        [StringLength(20)]
        public string ClasifefeDes { get; set; }
    }
}
