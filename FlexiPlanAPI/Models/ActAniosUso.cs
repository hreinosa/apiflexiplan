namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActAniosUso")]
    public partial class ActAniosUso
    {
        public long Id { get; set; }

        public short AniosUso { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Deducible { get; set; }

        [Column(TypeName = "numeric")]
        public decimal NoDeducible { get; set; }
    }
}
