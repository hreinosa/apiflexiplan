namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SolicitudExt")]
    public partial class SolicitudExt
    {
        public long Id { get; set; }

        public long IdDeudor { get; set; }

        public long IdDeudaExterna { get; set; }

        [Required]
        [StringLength(1)]
        public string Frecuencia { get; set; }

        public long Monto { get; set; }

        public long CuotaCredito { get; set; }

        public DateTime FechaCredito { get; set; }

        public virtual DeudaExterna DeudaExterna { get; set; }

        public virtual Deudor Deudor { get; set; }
    }
}
