namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvCombo")]
    public partial class InvCombo
    {
        public long Id { get; set; }

        public long IdInvProducto { get; set; }

        public long IdInvProdCombo { get; set; }

        public bool Primario { get; set; }

        public bool Opcional { get; set; }

        public bool Fijo { get; set; }

        public long IdEmpresa { get; set; }
    }
}
