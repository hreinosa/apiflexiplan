namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvDescEnc")]
    public partial class InvDescEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvDescEnc()
        {
            InvDescDet = new HashSet<InvDescDet>();
        }

        public long Id { get; set; }

        public long IdPuntoVenta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Numero { get; set; }

        public DateTime? Fecha { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvDescDet> InvDescDet { get; set; }

        public virtual PuntoVenta PuntoVenta { get; set; }
    }
}
