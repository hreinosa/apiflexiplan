namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvColor")]
    public partial class InvColor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvColor()
        {
            InvExistencia = new HashSet<InvExistencia>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(40)]
        public string InvColorDes { get; set; }

        public bool Multicolor { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvExistencia> InvExistencia { get; set; }
    }
}
