namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConPresupuestoEnc")]
    public partial class ConPresupuestoEnc
    {
        public long Id { get; set; }

        public long IdEmpresa { get; set; }

        public long IdEjercicio { get; set; }
    }
}
