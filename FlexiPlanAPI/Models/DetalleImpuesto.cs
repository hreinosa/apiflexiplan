namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetalleImpuesto")]
    public partial class DetalleImpuesto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DetalleImpuesto()
        {
            PlanXDetalleImpuesto = new HashSet<PlanXDetalleImpuesto>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(10)]
        public string CodImpuesto { get; set; }

        [Required]
        [StringLength(60)]
        public string DesImpuesto { get; set; }

        public short Tipo { get; set; }

        public long IdBasadoEn { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoGraMin { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoGraMax { get; set; }

        public bool ImprimirEnDocumento { get; set; }

        public DateTime? FechaInicial { get; set; }

        public DateTime? FechaFinal { get; set; }

        public bool Inactivo { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlanXDetalleImpuesto> PlanXDetalleImpuesto { get; set; }
    }
}
