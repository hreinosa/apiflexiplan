namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Parientes
    {
        public long Id { get; set; }

        public long IdCliente { get; set; }

        public long IdParentezco { get; set; }

        [StringLength(100)]
        public string Nombre { get; set; }

        public DateTime? FechaNac { get; set; }

        public bool Beneficiario { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Porcentaje { get; set; }

        [Required]
        [StringLength(1)]
        public string Sexo { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual Parentezco Parentezco { get; set; }
    }
}
