namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class rptFiltros
    {
        public long Id { get; set; }

        public long IdOpcion { get; set; }

        public byte? Orden { get; set; }

        [StringLength(25)]
        public string NombreCampo { get; set; }

        [StringLength(25)]
        public string CampoDescriptivo { get; set; }

        [StringLength(25)]
        public string TituloCampo { get; set; }

        [StringLength(1)]
        public string TipodeDato { get; set; }

        public bool UsarBoton { get; set; }

        public bool UsarRango { get; set; }

        [StringLength(600)]
        public string SqlBoton { get; set; }

        [StringLength(100)]
        public string TitulosBusqueda { get; set; }

        [StringLength(100)]
        public string AnchosBusqueda { get; set; }

        [StringLength(25)]
        public string CampoAMostrar { get; set; }

        [StringLength(20)]
        public string SqlTableAlias { get; set; }

        public long IdPadre { get; set; }

        public bool EsParametro { get; set; }
    }
}
