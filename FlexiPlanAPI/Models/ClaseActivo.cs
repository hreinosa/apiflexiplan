namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ClaseActivo")]
    public partial class ClaseActivo
    {
        public long id { get; set; }

        [Required]
        [StringLength(50)]
        public string ClaseActivodes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Residuo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VidaUtil { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdCatalogo3 { get; set; }

        public long IdCatalogo4 { get; set; }

        public long IdCatalogo5 { get; set; }

        public long IdCatalogo6 { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCatalogoCC { get; set; }
    }
}
