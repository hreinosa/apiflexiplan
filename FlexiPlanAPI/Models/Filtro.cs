namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Filtro")]
    public partial class Filtro
    {
        public double? Id { get; set; }

        public double? IdOpcion { get; set; }

        public double? Orden { get; set; }

        [StringLength(255)]
        public string NombreCampo { get; set; }

        [StringLength(255)]
        public string TituloCampo { get; set; }

        [StringLength(255)]
        public string TipodeDato { get; set; }

        [Key]
        [Column(Order = 0)]
        public bool UsarBoton { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool UsarRango { get; set; }

        [StringLength(255)]
        public string SqlBoton { get; set; }

        [StringLength(255)]
        public string TitulosBusqueda { get; set; }

        [StringLength(255)]
        public string AnchosBusqueda { get; set; }

        [StringLength(255)]
        public string CampoAMostrar { get; set; }

        [StringLength(255)]
        public string SqlTableAlias { get; set; }
    }
}
