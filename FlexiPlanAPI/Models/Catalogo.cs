namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Catalogo")]
    public partial class Catalogo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Catalogo()
        {
            CatalogoOpcion = new HashSet<CatalogoOpcion>();
            CatalogoxCatalogoCC = new HashSet<CatalogoxCatalogoCC>();
            CuentaBanco = new HashSet<CuentaBanco>();
            DiarioDet = new HashSet<DiarioDet>();
            EstResConfig = new HashSet<EstResConfig>();
            PresupConfigDet = new HashSet<PresupConfigDet>();
            Presupuesto = new HashSet<Presupuesto>();
            SaldoConta = new HashSet<SaldoConta>();
            TipoAhorro = new HashSet<TipoAhorro>();
            TipoCredito = new HashSet<TipoCredito>();
        }

        public long Id { get; set; }

        public long IdTipoCuenta { get; set; }

        public long IdPadre { get; set; }

        [Required]
        [StringLength(20)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        public string CatalogoDes { get; set; }

        public bool TipoSaldo { get; set; }

        public bool GralDeta { get; set; }

        public bool Inactiva { get; set; }

        public long IdEmpresa { get; set; }

        [Required]
        [StringLength(20)]
        public string CodigoPadre { get; set; }

        public long IdCatalogoCC { get; set; }

        public byte Nivel { get; set; }

        public long IdClasifEfe { get; set; }

        public long IdTipoActividad { get; set; }

        public virtual TipoCuenta TipoCuenta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatalogoOpcion> CatalogoOpcion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatalogoxCatalogoCC> CatalogoxCatalogoCC { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CuentaBanco> CuentaBanco { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiarioDet> DiarioDet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstResConfig> EstResConfig { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PresupConfigDet> PresupConfigDet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Presupuesto> Presupuesto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaldoConta> SaldoConta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipoAhorro> TipoAhorro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipoCredito> TipoCredito { get; set; }
    }
}
