namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoMovIVA")]
    public partial class TipoMovIVA
    {
        [Key]
        [Column(Order = 0)]
        public long Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdCatalogo { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(30)]
        public string TipoMovIVADes { get; set; }
    }
}
