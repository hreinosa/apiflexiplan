namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvDivision")]
    public partial class InvDivision
    {
        public long Id { get; set; }

        [Required]
        [StringLength(2)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(30)]
        public string InvDivisionDes { get; set; }

        public long IdEmpresa { get; set; }
    }
}
