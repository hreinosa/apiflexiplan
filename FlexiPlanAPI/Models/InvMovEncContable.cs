namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvMovEncContable")]
    public partial class InvMovEncContable
    {
        public long Id { get; set; }

        public long IdInvMovEnc { get; set; }

        public long IdEmpresa { get; set; }

        public short Secuencia { get; set; }

        public long IdCuenta { get; set; }

        [Required]
        [StringLength(20)]
        public string CodigoCuenta { get; set; }

        [StringLength(100)]
        public string DescripcionCuenta { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Debe { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Haber { get; set; }

        public bool Contabilizado { get; set; }

        public long IdUsuario { get; set; }

        [StringLength(600)]
        public string Concepto { get; set; }

        [StringLength(20)]
        public string Documento { get; set; }

        public long IdOpcion { get; set; }
    }
}
