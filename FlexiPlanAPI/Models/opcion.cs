namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("opcion")]
    public partial class opcion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public opcion()
        {
            CatalogoOpcion = new HashSet<CatalogoOpcion>();
        }

        public long id { get; set; }

        public long IdAppProject { get; set; }

        public long idpadre { get; set; }

        [Required]
        [StringLength(50)]
        public string opciontipo { get; set; }

        [Required]
        [StringLength(50)]
        public string opciondes { get; set; }

        [StringLength(50)]
        public string programa { get; set; }

        [StringLength(1)]
        public string vbindex { get; set; }

        [Required]
        [StringLength(5)]
        public string Posicion { get; set; }

        [StringLength(300)]
        public string Ico { get; set; }

        public bool activo { get; set; }

        [StringLength(50)]
        public string filtro { get; set; }

        [StringLength(8000)]
        public string SeleccionSQL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatalogoOpcion> CatalogoOpcion { get; set; }
    }
}
