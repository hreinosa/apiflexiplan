namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rrhEmpleado")]
    public partial class rrhEmpleado
    {
        public long Id { get; set; }

        [Required]
        [StringLength(15)]
        public string PrimerNombre { get; set; }

        [StringLength(15)]
        public string SegundoNombre { get; set; }

        [Required]
        [StringLength(30)]
        public string Apellidos { get; set; }

        [StringLength(15)]
        public string ApellidoCasada { get; set; }

        [StringLength(65)]
        public string Direccion { get; set; }

        [StringLength(15)]
        public string Telefono1 { get; set; }

        [StringLength(15)]
        public string Telefono2 { get; set; }

        [StringLength(35)]
        public string CorreoElectronico { get; set; }

        public long IdSucursal { get; set; }

        [StringLength(10)]
        public string IdDepartamento { get; set; }

        public long IdrrhCargo { get; set; }

        public long IdrrhSupervisor { get; set; }

        public bool EsVendedor { get; set; }

        public bool EsCobrador { get; set; }

        public long IdUsuario { get; set; }
    }
}
