namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EstResultado")]
    public partial class EstResultado
    {
        public long Id { get; set; }

        public long IdPadre { get; set; }

        public long IdCatalogo { get; set; }

        [Required]
        [StringLength(50)]
        public string EstResultadoDes { get; set; }

        public short Orden { get; set; }

        public bool Incluir { get; set; }

        public short Signo { get; set; }

        public long IdEmpresa { get; set; }
    }
}
