namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActivoFijo")]
    public partial class ActivoFijo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ActivoFijo()
        {
            Depreciacion = new HashSet<Depreciacion>();
        }

        public long id { get; set; }

        public long IdActivoPadre { get; set; }

        [Required]
        [StringLength(10)]
        public string Codigo { get; set; }

        public long idTipoActivo { get; set; }

        public long IdClaseActivo { get; set; }

        public long IdSucursal { get; set; }

        public long IdDepto { get; set; }

        [Required]
        [StringLength(254)]
        public string ActivoFijoDes { get; set; }

        [StringLength(500)]
        public string Ubicacion { get; set; }

        [Required]
        [StringLength(20)]
        public string NumSerie { get; set; }

        public DateTime FechaAdqui { get; set; }

        [StringLength(254)]
        public string Fuente { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorOriginal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ValorActual { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VidaUtil { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Residuo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal DepreAnual { get; set; }

        [Column(TypeName = "numeric")]
        public decimal DepreMensual { get; set; }

        public bool liquidado { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdCatalogo3 { get; set; }

        public long IdCatalogo4 { get; set; }

        public long IdCatalogo5 { get; set; }

        public long IdCatalogo6 { get; set; }

        public long IdCatalogoCC { get; set; }

        public long IdcaAvion { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VidaUtilActual { get; set; }

        public DateTime? FechaAdicion { get; set; }

        public long IdActAniosUso { get; set; }

        [StringLength(30)]
        public string Marca { get; set; }

        [StringLength(30)]
        public string Modelo { get; set; }

        public bool Inactivo { get; set; }

        public DateTime? FechaRetiro { get; set; }

        [StringLength(255)]
        public string MotivoRetiro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Depreciacion> Depreciacion { get; set; }
    }
}
