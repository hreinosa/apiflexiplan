namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Banco")]
    public partial class Banco
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Banco()
        {
            CuentaBanco = new HashSet<CuentaBanco>();
        }

        public long Id { get; set; }

        [StringLength(30)]
        public string BancoDes { get; set; }

        [StringLength(4)]
        public string NombreCorto { get; set; }

        [StringLength(4)]
        public string CodCampero { get; set; }

        [StringLength(1000)]
        public string Formulario { get; set; }

        [StringLength(1000)]
        public string Formulario2 { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CuentaBanco> CuentaBanco { get; set; }
    }
}
