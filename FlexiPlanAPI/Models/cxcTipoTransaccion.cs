namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cxcTipoTransaccion")]
    public partial class cxcTipoTransaccion
    {
        public long Id { get; set; }

        [Column("cxcTipoTransaccion")]
        [Required]
        [StringLength(20)]
        public string cxcTipoTransaccion1 { get; set; }

        public short Signo { get; set; }

        [Required]
        [StringLength(20)]
        public string Numero { get; set; }
    }
}
