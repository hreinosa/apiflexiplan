namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MovimientoBanco")]
    public partial class MovimientoBanco
    {
        public long Id { get; set; }

        public long IdPeriodo { get; set; }

        public long IdTipoMovBanco { get; set; }

        public long IdCuentaBanco { get; set; }

        public long IdDiarioEnc { get; set; }

        public long? IdCliente { get; set; }

        public long Numero { get; set; }

        public DateTime Fecha { get; set; }

        [StringLength(600)]
        public string Concepto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Monto { get; set; }

        public bool Impreso { get; set; }

        public bool Anulado { get; set; }

        public bool Posteado { get; set; }

        public bool Conciliado { get; set; }

        [StringLength(100)]
        public string Paguese { get; set; }

        public long IdEmpresa { get; set; }

        [StringLength(15)]
        public string NDocumentoBanco { get; set; }

        public long IdBanConciliacion { get; set; }

        public virtual CuentaBanco CuentaBanco { get; set; }

        public virtual Periodo Periodo { get; set; }

        public virtual TipoMovBanco TipoMovBanco { get; set; }
    }
}
