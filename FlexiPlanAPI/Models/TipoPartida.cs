namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoPartida")]
    public partial class TipoPartida
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoPartida()
        {
            DiarioEnc = new HashSet<DiarioEnc>();
            TipoMovBanco = new HashSet<TipoMovBanco>();
        }

        public long Id { get; set; }

        [StringLength(20)]
        public string TipoPartidaDes { get; set; }

        public bool Cierre { get; set; }

        public bool Liquidacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiarioEnc> DiarioEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipoMovBanco> TipoMovBanco { get; set; }
    }
}
