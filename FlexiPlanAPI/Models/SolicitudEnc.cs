namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SolicitudEnc")]
    public partial class SolicitudEnc
    {
        public long Id { get; set; }

        public long IdTipoCredito { get; set; }

        public long IdCortePago { get; set; }

        [Required]
        [StringLength(1)]
        public string Frecuencia { get; set; }

        public long Numero { get; set; }

        public long IdSucursal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaInteres { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaSeguro { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaMora { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Monto { get; set; }

        public short CantCuotas { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Cuota { get; set; }

        public DateTime FecInicio { get; set; }

        public DateTime? FecFinal { get; set; }

        public bool Aprobada { get; set; }

        public virtual CortePago CortePago { get; set; }

        public virtual Sucursal Sucursal { get; set; }

        public virtual TipoCredito TipoCredito { get; set; }
    }
}
