namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CatalogoCC")]
    public partial class CatalogoCC
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CatalogoCC()
        {
            CatalogoxCatalogoCC = new HashSet<CatalogoxCatalogoCC>();
            SaldoCC = new HashSet<SaldoCC>();
        }

        public long Id { get; set; }

        public long IdTipoCuenta { get; set; }

        public long IdPadre { get; set; }

        [Required]
        [StringLength(20)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        public string CatalogoDes { get; set; }

        public bool TipoSaldo { get; set; }

        public bool GralDeta { get; set; }

        public bool Inactiva { get; set; }

        public long IdEmpresa { get; set; }

        [Required]
        [StringLength(20)]
        public string CodigoPadre { get; set; }

        public byte Nivel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatalogoxCatalogoCC> CatalogoxCatalogoCC { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SaldoCC> SaldoCC { get; set; }
    }
}
