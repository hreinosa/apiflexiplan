namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvExistencia")]
    public partial class InvExistencia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvExistencia()
        {
            InvDescDet = new HashSet<InvDescDet>();
            InvFisDet = new HashSet<InvFisDet>();
        }

        public long Id { get; set; }

        public long IdSucursal { get; set; }

        public long IdInvProducto { get; set; }

        public long IdInvClasif { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Costo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Precio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Existencia { get; set; }

        public long? IdInvMovEnc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaDescuento { get; set; }

        [StringLength(20)]
        public string CodigoBarra { get; set; }

        public long IdInvColor { get; set; }

        public long IdInvTalla { get; set; }

        [StringLength(20)]
        public string OldCode { get; set; }

        [StringLength(80)]
        public string Info { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaPrecio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaCosto { get; set; }

        public long IdEmpresa { get; set; }

        public virtual InvColor InvColor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvDescDet> InvDescDet { get; set; }

        public virtual InvTalla InvTalla { get; set; }

        public virtual Sucursal Sucursal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvFisDet> InvFisDet { get; set; }
    }
}
