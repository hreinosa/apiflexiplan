namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Departamento")]
    public partial class Departamento
    {
        public long id { get; set; }

        public long? idCtaDepto { get; set; }

        [Required]
        [StringLength(100)]
        public string DeptoDes { get; set; }

        public long IdEmpresa { get; set; }
    }
}
