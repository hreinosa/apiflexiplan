namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvDescDet")]
    public partial class InvDescDet
    {
        public long Id { get; set; }

        public long IdInvDescEnc { get; set; }

        public long IdInvExistencia { get; set; }

        public short Secuencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Cantidad { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Precio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaDescuento { get; set; }

        public long IdEmpresa { get; set; }

        public virtual InvDescEnc InvDescEnc { get; set; }

        public virtual InvExistencia InvExistencia { get; set; }
    }
}
