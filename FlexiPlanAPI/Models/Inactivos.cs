namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Inactivos
    {
        public long Id { get; set; }

        public long IdCliente { get; set; }

        public long IdCortePago { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual CortePago CortePago { get; set; }
    }
}
