namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CatalogoxCatalogoCC")]
    public partial class CatalogoxCatalogoCC
    {
        public long Id { get; set; }

        public long IdCatalogo { get; set; }

        public long IdCatalogoCC { get; set; }

        public long IdEmpresa { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Porcentaje { get; set; }

        public virtual Catalogo Catalogo { get; set; }

        public virtual CatalogoCC CatalogoCC { get; set; }
    }
}
