namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SaldoBanco")]
    public partial class SaldoBanco
    {
        public long Id { get; set; }

        public long IdPeriodo { get; set; }

        public long IdCuentaBanco { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoInicial { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoIngreso { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoEgreso { get; set; }

        public long IdEmpresa { get; set; }

        public virtual CuentaBanco CuentaBanco { get; set; }

        public virtual Periodo Periodo { get; set; }
    }
}
