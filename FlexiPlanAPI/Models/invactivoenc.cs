namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("invactivoenc")]
    public partial class invactivoenc
    {
        public long id { get; set; }

        public long idusuario { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime fecha { get; set; }

        [StringLength(300)]
        public string descripcion { get; set; }

        public bool cerrado { get; set; }

        public long IdEmpresa { get; set; }
    }
}
