namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoMovBanco")]
    public partial class TipoMovBanco
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoMovBanco()
        {
            MovimientoBanco = new HashSet<MovimientoBanco>();
        }

        public long Id { get; set; }

        [StringLength(20)]
        public string TipoMovBancoDes { get; set; }

        public bool EsCheque { get; set; }

        public int Signo { get; set; }

        public long IdTipoPartida { get; set; }

        public bool EsRemesa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MovimientoBanco> MovimientoBanco { get; set; }

        public virtual TipoPartida TipoPartida { get; set; }
    }
}
