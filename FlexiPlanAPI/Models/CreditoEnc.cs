namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CreditoEnc")]
    public partial class CreditoEnc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CreditoEnc()
        {
            CreditoDet = new HashSet<CreditoDet>();
            SolicitudDet = new HashSet<SolicitudDet>();
        }

        public long Id { get; set; }

        public long IdSolicitud { get; set; }

        public long IdCliente { get; set; }

        public long IdTipoCredito { get; set; }

        public long IdCortePago { get; set; }

        public long IdSucursal { get; set; }

        public long IdStatus { get; set; }

        public long IdPeriodo { get; set; }

        [Required]
        [StringLength(20)]
        public string Numero { get; set; }

        public DateTime FechaCredito { get; set; }

        [Required]
        [StringLength(1)]
        public string Frecuencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaInteres { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaSeguro { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaMora { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoCredito { get; set; }

        public short CantCuotas { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoCuota { get; set; }

        public DateTime FecIniPag { get; set; }

        public DateTime? FecFinPag { get; set; }

        public DateTime? FecUltAnt { get; set; }

        public DateTime? FecSigPag { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoCapital { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoInteres { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoMora { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual CortePago CortePago { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditoDet> CreditoDet { get; set; }

        public virtual Periodo Periodo { get; set; }

        public virtual Status Status { get; set; }

        public virtual Sucursal Sucursal { get; set; }

        public virtual TipoCredito TipoCredito { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudDet> SolicitudDet { get; set; }
    }
}
