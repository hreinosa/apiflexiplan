namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Proyecto")]
    public partial class Proyecto
    {
        public long Id { get; set; }

        [StringLength(20)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(60)]
        public string ProyectoDes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PresMateriales { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PresManoObra { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PresOtroCosto { get; set; }

        public long IdSupervisor { get; set; }

        public long IdGeneral { get; set; }

        public long IdEjercicio { get; set; }

        public long IdDivision { get; set; }

        public long IdCliente { get; set; }

        [StringLength(1)]
        public string CodGeneral { get; set; }

        [StringLength(2)]
        public string CodAnio { get; set; }

        [StringLength(1)]
        public string CodDivision { get; set; }

        [StringLength(4)]
        public string CodCliente { get; set; }

        [StringLength(2)]
        public string Correlativo { get; set; }

        public bool Inactivo { get; set; }

        public bool Cerrado { get; set; }

        public long IdEmpresa { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
