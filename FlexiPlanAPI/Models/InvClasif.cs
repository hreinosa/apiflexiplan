namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvClasif")]
    public partial class InvClasif
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvClasif()
        {
            InvProducto = new HashSet<InvProducto>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(30)]
        public string InvClasifDes { get; set; }

        public bool Principal { get; set; }

        [StringLength(4)]
        public string Codigo { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvProducto> InvProducto { get; set; }
    }
}
