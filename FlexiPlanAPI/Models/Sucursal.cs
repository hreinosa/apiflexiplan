namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sucursal")]
    public partial class Sucursal
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Sucursal()
        {
            CreditoEnc = new HashSet<CreditoEnc>();
            cxpQuedanEnc = new HashSet<cxpQuedanEnc>();
            InvExistencia = new HashSet<InvExistencia>();
            InvFisEnc = new HashSet<InvFisEnc>();
            InvMovEnc = new HashSet<InvMovEnc>();
            PuntoVenta = new HashSet<PuntoVenta>();
            SolicitudEnc = new HashSet<SolicitudEnc>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(60)]
        public string SucursalDes { get; set; }

        public bool Primaria { get; set; }

        public long IdEmpresa { get; set; }

        [StringLength(65)]
        public string Direccion { get; set; }

        public long IdCiudad { get; set; }

        public long IdEstado { get; set; }

        public long IdPais { get; set; }

        [StringLength(15)]
        public string Telefono1 { get; set; }

        [StringLength(15)]
        public string Telefono2 { get; set; }

        public long IdPlanImpuestoCompra { get; set; }

        public long IdPlanImpuestoVenta { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdCatalogo3 { get; set; }

        public long IdCatalogo4 { get; set; }

        public long IdCatalogo5 { get; set; }

        public long IdCatalogo6 { get; set; }

        public long IdCatalogo7 { get; set; }

        public long IdCatalogo8 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditoEnc> CreditoEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cxpQuedanEnc> cxpQuedanEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvExistencia> InvExistencia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvFisEnc> InvFisEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvMovEnc> InvMovEnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PuntoVenta> PuntoVenta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SolicitudEnc> SolicitudEnc { get; set; }
    }
}
