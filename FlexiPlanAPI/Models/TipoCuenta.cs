namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoCuenta")]
    public partial class TipoCuenta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoCuenta()
        {
            Catalogo = new HashSet<Catalogo>();
        }

        public long ID { get; set; }

        [StringLength(60)]
        public string TipoCuentaDes { get; set; }

        public bool? Balance { get; set; }

        public bool? Resultado { get; set; }

        [StringLength(20)]
        public string NomCorto { get; set; }

        public long IdEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Catalogo> Catalogo { get; set; }
    }
}
