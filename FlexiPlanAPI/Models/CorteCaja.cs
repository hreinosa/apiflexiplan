namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CorteCaja")]
    public partial class CorteCaja
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CorteCaja()
        {
            MovCaja = new HashSet<MovCaja>();
        }

        public long Id { get; set; }

        public long IdPuntoVenta { get; set; }

        public DateTime Inicio { get; set; }

        public DateTime? Final { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoInicial { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Ingresos { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Egresos { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SaldoFinal { get; set; }

        public bool Cerrado { get; set; }

        public virtual PuntoVenta PuntoVenta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MovCaja> MovCaja { get; set; }
    }
}
