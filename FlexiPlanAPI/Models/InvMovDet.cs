namespace FlexiPlanAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InvMovDet")]
    public partial class InvMovDet
    {
        public long Id { get; set; }

        public long IdInvMovEnc { get; set; }

        public long IdInvProducto { get; set; }

        public long IdInvExistencia { get; set; }

        public long IdPadre { get; set; }

        public short Secuencia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Cantidad { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Costo { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Precio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaDescuento { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaIva { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaCosto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TasaPrecio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ExistAnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CostoAnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PrecioAnt { get; set; }

        public bool CambiaPrecio { get; set; }

        public bool Consignacion { get; set; }

        public bool Exento { get; set; }

        public bool NoSujeta { get; set; }

        public bool Autorizar { get; set; }

        public byte? Autorizado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CantBodega { get; set; }

        [StringLength(1000)]
        public string Concepto { get; set; }

        public long IdCatalogo { get; set; }

        public long IdEmpresa { get; set; }

        public bool Averia { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdCatalogo3 { get; set; }

        public long IdCatalogo4 { get; set; }

        public long IdCatalogo5 { get; set; }

        public long IdCatalogo6 { get; set; }

        public long IdCatalogo7 { get; set; }

        public long IdCatalogo8 { get; set; }

        public long IdInvListaPrecioDet { get; set; }

        public long IdInvNivelPrecio { get; set; }

        public long IdInvMedida { get; set; }

        public bool Servicio { get; set; }

        public bool IncluyeImpuesto { get; set; }

        public long IdDetalleImpuesto { get; set; }

        public long IdPlanImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Porcentaje { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PrecioUnitario { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PrecioNeto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotalImpuesto { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MontoTotal { get; set; }

        public long IdInvProductoPartes { get; set; }

        [StringLength(50)]
        public string NumeroParte { get; set; }

        [StringLength(50)]
        public string NumeroSerie { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ExistAntParte { get; set; }

        public long IdInvOrdenCompraDet { get; set; }

        public long IdSucursal { get; set; }

        public long IdSucursalDestino { get; set; }

        public virtual InvMovEnc InvMovEnc { get; set; }

        public virtual InvProducto InvProducto { get; set; }
    }
}
