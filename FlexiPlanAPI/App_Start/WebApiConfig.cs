﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FlexiPlanAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web
            //config.Formatters.Remove(config.Formatters.XmlFormatter);
            // Rutas de API web
            config.EnableCors();

            // Rutas de API web
            config.MapHttpAttributeRoutes();

            // Rutas estáticas para poder acceder a métodos get que no reciben parámetros en un mismo controller
            config.Routes.MapHttpRoute(
                name: "GetLugares",
                routeTemplate: "api/{controller}/GetLugares",
                defaults: new { action = "GetLugares" }
            );
            //Ruta para obtener Planes de Pago
            config.Routes.MapHttpRoute(
                name: "GetPlanes",
                routeTemplate: "api/{controller}/GetPlanes",
                defaults: new { action = "GetPlanes" }
            );
            //Ruta para obtener el listado de empresas
            config.Routes.MapHttpRoute(
                name: "GetEmpresas",
                routeTemplate: "api/{controller}/GetEmpresas",
                defaults: new { action = "GetEmpresas" }
            );
            //Ruta para almacenar contactos
            config.Routes.MapHttpRoute(
               name: "StoreContact",
               routeTemplate: "api/{controller}/StoreContact",
               defaults: new { action = "StoreContact" }
           );
            //Ruta para verificar si un usario es supervisor
            config.Routes.MapHttpRoute(
                name: "GetRRHHProfile",
                routeTemplate: "api/{controller}/EsSupervisor/{id}",
                defaults: new {action = "EsSupervisor"}
            );
            //Ruta para obtener el listado de ids de los vendedores
            config.Routes.MapHttpRoute(
                name: "GetRelatedSellers",
                routeTemplate: "api/{controller}/GetSellers/{id}",
                defaults: new {action = "GetSellers"}
                );
            //Ruta para obtener la información de un Vendedor
            config.Routes.MapHttpRoute(
                name: "GetSellerData",
                routeTemplate: "api/{controller}/GetSellerData/{id}",
                defaults: new {action = "GetSellerData"}
                );
            
            //Ruta para obtener el listado de categorías de productos
            config.Routes.MapHttpRoute(
                name: "GetCategorias",
                routeTemplate: "api/{controller}/GetCategories",
                defaults: new { action = "GetCategories" });

            //Ruta para obtener el listado de productos pertenecientes a una categoría
            config.Routes.MapHttpRoute(
                name: "GetProductsByCat",
                routeTemplate: "api/{controller}/GetProductsByCat/{id}",
                defaults: new { action = "GetProductsByCat" });


            //RUTAS NUEVAS

            //Ruta para obtener un producto
            config.Routes.MapHttpRoute(
                name: "GetProduct",
                routeTemplate: "api/{controller}/GetProduct/{id}/{codigo}",
                defaults: new { action = "GetProduct" });


            //Ruta para obtener las sucursales
            config.Routes.MapHttpRoute(
                name: "GetSucursales",
                routeTemplate: "api/{controller}/GetSucursales",

                defaults: new { action = "GetSucursales" }
                );


            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                 name: "GetCategoria",
                 routeTemplate: "api/{controller}/GetCategoria/{id}",
                 defaults: new { action = "GetCategoria" });




        }
    }
}
