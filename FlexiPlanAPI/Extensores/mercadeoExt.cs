﻿using FlexiPlanAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlexiPlanAPI.Extensores
{
    public class mercadeoExt
    {
        private Model db = new Model();
        public List<SucursalP> GetSucursales()
        {
            var suc = db.Sucursal.ToList();

            List<SucursalP> sucursales = new List<SucursalP>();

            foreach (var s in suc)
            {
                sucursales.Add(new SucursalP {
                    Id =s.Id,
                    SucursalDes = s.SucursalDes,
                    IdEmpresa = s.IdEmpresa,
                    Direccion = s.Direccion,
                    IdCiudad = s.IdCiudad,
                    IdEstado = s.IdEstado,
                    Telefono1 = s.Telefono1
                    
                });
            }
            return sucursales;
        }

        public List<CategoriaP> GetCategorias()
        {
            var categorias = db.InvClase.ToList();

            List<CategoriaP> categoriasP = new List<CategoriaP>();

            foreach (var item in categorias)
            {
                categoriasP.Add(new CategoriaP { Id = item.Id, InvClaseDes = item.InvClaseDes, Codigo = item.Codigo, IdEmpresa = item.IdEmpresa });
            }


            return categoriasP;
        }

        public List<InvProductoP>GetProductos(long id)
        {
            var productos = db.InvProducto.Where(p => p.IdInvClase == id).ToList();
            List<InvProductoP> ProductosCat = new List<InvProductoP>();

            foreach (var item in productos)
            {
                ProductosCat.Add(new InvProductoP { Id = item.Id, InvProductoDes = item.InvProductoDes, Codigo = item.Codigo, Serie = item.Serie, Costo = item.Costo });
            }
            return ProductosCat;
        }

        //NUEVO METODO PARA OBTENER UN PRODUCTO
        public List<InvProductoP> GetProducto(long id, string codigo)
        {
            var producto = db.InvProducto.Where(p => p.IdInvClase == id).Where(p => p.Codigo == codigo).ToList();
            List<InvProductoP> ProductoCat = new List<InvProductoP>();

            foreach (var item in producto)
            {
                ProductoCat.Add(new InvProductoP { Id = item.Id, InvProductoDes = item.InvProductoDes, Codigo = item.Codigo, Serie = item.Serie, Costo = item.Costo });
            }
            return ProductoCat;
        }


        public List<CategoriaP> GetCategoria(long id)
        {
            var categorias = db.InvClase.Where(p => p.Id == id).ToList();

            List<CategoriaP> categoriasP = new List<CategoriaP>();

            foreach (var item in categorias)
            {
                categoriasP.Add(new CategoriaP { Id = item.Id, InvClaseDes = item.InvClaseDes, Codigo = item.Codigo, IdEmpresa = item.IdEmpresa });
            }

            return categoriasP;
        }




    }
    #region ClasesPersonalizadas

    public class CategoriaP
    {
        public long Id { get; set; }
        [Required]
        [StringLength(40)]
        public string InvClaseDes { get; set; }

        [StringLength(4)]
        public string Codigo { get; set; }

        public long IdEmpresa { get; set; }
    }
    public class InvProductoP
    {

        public long Id { get; set; }

        public long IdInvClase { get; set; }

        public long IdInvDivision { get; set; }

        public long IdInvMarca { get; set; }

        public long IdInvModelo { get; set; }

        public long? IdProveedor { get; set; }

        public long IdInvGrupo { get; set; }

        public long IdInvClasif { get; set; }

        public long? IdInvMedida { get; set; }

        [Required]
        [StringLength(25)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(200)]
        public string InvProductoDes { get; set; }

        public bool Serie { get; set; }

        public bool Exento { get; set; }

        public bool Combo { get; set; }

        public bool Servicio { get; set; }

        public bool Consignacion { get; set; }

        [StringLength(100)]
        public string NomComercial { get; set; }

        [StringLength(100)]
        public string ConocidoPor { get; set; }

        
        public decimal Costo { get; set; }

        public decimal Existencia { get; set; }

        public long IdEmpresa { get; set; }

        public long IdCatalogo1 { get; set; }

        public long IdCatalogo2 { get; set; }

        public long IdCatalogo3 { get; set; }

        public long IdCatalogo4 { get; set; }

        public long IdCatalogo5 { get; set; }

        public long IdCatalogo6 { get; set; }

        public long IdCatalogo7 { get; set; }

        public long IdCatalogo8 { get; set; }
    }
    public class SucursalP
    {
        public long Id { get; set; }
        public string SucursalDes { get; set; }
        public long IdEmpresa { get; set; }
        public string Direccion { get; set; }
        public long IdCiudad { get; set; }
        public long IdEstado { get; set; }
        public string Telefono1 { get; set; }
    }
    #endregion 

}