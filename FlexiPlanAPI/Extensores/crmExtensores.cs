﻿using FlexiPlanAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlexiPlanAPI.Extensores
{
    public class crmExtensores
    {
        private Model db = new Model();
        public List<VendedorP> getEmpleados()
        {
            var empleados = db.rrhEmpleado.Where(em => em.EsVendedor == true);
            List<VendedorP> vendedoresP = new List<VendedorP>();

            foreach (var e in empleados)
            {
                vendedoresP.Add(new VendedorP { id = e.Id, firstName = e.PrimerNombre, secondName = e.SegundoNombre, lastNames = e.Apellidos, esVendedor = e.EsVendedor });
            }

            return vendedoresP;
        }


        public VendedorP getVendedor(int id)
        {
            var v = db.rrhEmpleado.Where(ven => ven.Id == id).FirstOrDefault();
            VendedorP vendedor = new VendedorP();
            vendedor.id = v.Id;
            vendedor.firstName = v.PrimerNombre;
            vendedor.secondName = v.SegundoNombre;
            vendedor.lastNames = v.Apellidos;

            return vendedor;
        }

    }

   

    public class VendedorP
    {
        public long id { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string lastNames { get; set; }

        public bool esVendedor { get; set; }
    }
}