﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlexiPlanAPI.Models;
using Microsoft.VisualBasic;
using Newtonsoft.Json;

namespace FlexiPlanAPI.Extensores
{
    
    public class LoginExt
    {
        private Model db = new Model();
        //Metodo que comprueba si el usuario existe en la base de datos
        public bool userExists(string user)
        {
            Usuario usuario = new Usuario();
            usuario = db.Usuario.Where( u=> u.Codigo == user).FirstOrDefault();

            if (usuario == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Metodo que obtiene el registro completo del usuario en la tabla USUARIO
        public Usuario GetUsuario(string user)
        {
            var usuario = db.Usuario.Where(u => u.Codigo == user).FirstOrDefault();

            return usuario;
        }

        //Método que encripta la contraseña con  sintaxis Visual Basic
        public string bcrypt (string pass)
        {
            string Letr="";
            string bcrypt = "";

            for (int x = 1; x <= pass.Length; x++)
            {
                Letr = Strings.Mid(pass, x, 1);
                bcrypt = bcrypt + Strings.Chr((Strings.Asc(Letr) - 1) * 2);
            }

            return bcrypt;

        }

        //Método que desencripta la contraseña con sintaxis Visual Basic
        public string decrypt(string pass)
        {

            string Letr="";

            string decrypt = "";

            for (int x = 1; x <= pass.Length; x++)
            {
                Letr = Strings.Mid(pass, x, 1);
                decrypt = decrypt + Strings.Chr((Strings.Asc(Letr)/2)+1);
            }

            return decrypt;

        }

        public CustomUsers  FindByUser(string user)
        {
            var u = GetUsuario(user);

            if (u != null)
            {
                CustomUsers userToSend = new CustomUsers();

                userToSend.id = u.id;
                userToSend.idperfilenc = u.PerfilEnc.id;
                userToSend.Codigo = u.Codigo;
                userToSend.NombreCompleto = u.NombreCompleto;
                userToSend.clave = decrypt(u.clave);
                userToSend.SubUsuario = u.SubUsuario;
                userToSend.SubIndex = u.SubIndex;
                userToSend.activo = u.activo;
                userToSend.Logged = u.Logged;
                userToSend.Sessions = u.Sessions;
                userToSend.Autorizado = u.Autorizado;
                userToSend.KeyVenta = u.KeyVenta;
                userToSend.SobreFac = u.SobreFac;
                userToSend.EsComprador = u.EsComprador;
                userToSend.EsVendedor = u.EsVendedor;
                userToSend.EsCajero = u.EsCajero;
                userToSend.AutorizaCompras = u.AutorizaCompras;
                userToSend.email = u.email;

                var Usuario = JsonConvert.SerializeObject(userToSend);

                return userToSend;
            }
            else
            {
                return null;
            }
            
        }

        public CustomUsers FindByEmail(string user)
        {
            var u = db.Usuario.Where(us => us.email == user).FirstOrDefault();

            if (u != null)
            {
                CustomUsers userToSend = new CustomUsers();

                userToSend.id = u.id;
                userToSend.idperfilenc = u.PerfilEnc.id;
                userToSend.Codigo = u.Codigo;
                userToSend.NombreCompleto = u.NombreCompleto;
                userToSend.clave = decrypt(u.clave);
                userToSend.SubUsuario = u.SubUsuario;
                userToSend.SubIndex = u.SubIndex;
                userToSend.activo = u.activo;
                userToSend.Logged = u.Logged;
                userToSend.Sessions = u.Sessions;
                userToSend.Autorizado = u.Autorizado;
                userToSend.KeyVenta = u.KeyVenta;
                userToSend.SobreFac = u.SobreFac;
                userToSend.EsComprador = u.EsComprador;
                userToSend.EsVendedor = u.EsVendedor;
                userToSend.EsCajero = u.EsCajero;
                userToSend.AutorizaCompras = u.AutorizaCompras;
                userToSend.email = u.email;

                var Usuario = JsonConvert.SerializeObject(userToSend);

                return userToSend;
            }
            else
            {
                return null;
            }

        }

    }
}