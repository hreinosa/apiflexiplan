﻿using FlexiPlanAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FlexiPlanAPI.Extensores
{
    public class ContactosExt
    {
        private Model db = new Model();

        //Metodo auxiliar para obtener lugares de trabajo
        public List<LugaresP> Lugares()
        {
            var lug = db.LugarTrabajo.ToList();

            List<LugaresP> lugares = new List<LugaresP>();

            foreach (var item in lug)
            {
                lugares.Add(new LugaresP { Id = item.Id, Codigo = item.Codigo, LugarTrabajoDes = item.LugarTrabajoDes });
            }
            return lugares;
        }

        //Método auxiliar para obtener planes de impuestos
        public List<PlanesP> Planes()
        {
            var pl = db.PlanImpuesto.ToList();

            List<PlanesP> planes = new List<PlanesP>();

            foreach (var item in pl)
            {
                planes.Add(new PlanesP { Id = item.Id, CodPlanImpuesto = item.CodPlanImpuesto, DesPlanImpuesto = item.DesPlanImpuesto, IdEmpresa = item.IdEmpresa });
            }
            return planes;
        }

        //Método auxiliar para obtener Empresas
        public List<EmpresaP> Empresas()
        {
            var em = db.Empresa.ToList();

            List<EmpresaP> empresas = new List<EmpresaP>();
            foreach (var item in em)
            {
                empresas.Add(new EmpresaP
                {
                    Id = item.Id,
                    Nombre = item.EmpresaDes
                });
            }
            return empresas;
        }

        //Método para obtener el perfil del  usuario en sesión en la tabla rrhhEmpleados
        public rrhEmpleado Perfil(int id)
        {
            var perfil = db.rrhEmpleado.Where(per => per.IdUsuario == id);
            return perfil.FirstOrDefault();
        }

        //Método que retorna los id de los vendedores relacionados con un supervisor.
        public long[] RelatedSellers(int id)
        {
            rrhEmpleado perfil = db.rrhEmpleado.Where(per => per.IdUsuario == id).FirstOrDefault();
            var all = db.rrhEmpleado.ToList();

            var query = (from item in all where item.IdrrhSupervisor == perfil.IdrrhSupervisor select item.Id).ToArray();
            return query;
        }

        //Método que retorna los datos de un vendedor, basado en su ID de usuario
        public Object GetSellerData(long id)
        {
            var empleado = db.rrhEmpleado.Where(e => e.IdUsuario == id).FirstOrDefault();
            if (empleado != null)
            {
                return empleado;
            }
            else
            {
                return null;
            }
        }



    }

    #region
    public class LugaresP
    {
        public long Id { get; set; }

        public int Codigo { get; set; }

        [StringLength(40)]
        public string LugarTrabajoDes { get; set; }

        public bool EsProv { get; set; }

        public bool EsPart { get; set; }

        public bool EsAcreedor { get; set; }

        public bool EsEmpleado { get; set; }
    }
    public class PlanesP
    {
        public long Id { get; set; }

        [Required]
        [StringLength(10)]
        public string CodPlanImpuesto { get; set; }

        [Required]
        [StringLength(60)]
        public string DesPlanImpuesto { get; set; }

        public long IdEmpresa { get; set; }
    }

    public class EmpresaP
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
    }

    #endregion

}