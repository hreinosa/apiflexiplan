﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlexiPlanAPI.Extensores
{
    public class CustomUsers
    {
        public long id { get; set; }
        public long idperfilenc { get; set; }
        public string Codigo { get; set; }
        public string NombreCompleto { get; set; }
        public string clave { get; set; }
        public string SubUsuario { get; set; }
        public string SubIndex { get; set; }
        public bool activo { get; set; }
        public bool Logged { get; set; }
        public int Sessions { get; set; }
        public bool Autorizado { get; set; }
        public string KeyVenta { get; set; }
        public bool SobreFac { get; set; }
        public bool EsComprador { get; set; }
        public bool EsVendedor { get; set; }
        public bool EsCajero { get; set; }
        public bool AutorizaCompras { get; set; }
        public string email { get; set; }

    }
}