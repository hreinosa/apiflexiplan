﻿using FlexiPlanAPI.Extensores;
using FlexiPlanAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FlexiPlanAPI.Controllers
{
    public class ContactosController : ApiController
    {
        public Model db = new Model();
        
        ContactosExt c = new ContactosExt();
        
        [HttpGet]
        public Object GetLugares()
        {
            return c.Lugares();
        }

        [HttpGet]
        public Object GetPlanes()
        {
            return c.Planes();
        }

        [HttpGet]
        public Object GetEmpresas()
        {
            return c.Empresas();
        }


        //Método que comprueba si el usuario en sesión de Laravel es Supervisor
        [HttpGet]
        public bool EsSupervisor(int id)
        {
            bool EsSupervisor;
            
            var p = c.Perfil(id);
            if (p == null)
            {
                return false;
            }
            else
            {
                if (p.IdrrhCargo == 2)
                {
                    EsSupervisor = true;
                    return EsSupervisor;
                }
                else
                {
                    return false;
                }
            }
        }


        //Método que obtiene los id de los vendedores relacionados con un supervisor
        [HttpGet]
        public long[] GetSellers(int id)
        {
            return c.RelatedSellers(id);
        }

        //Método que retorna el nombre del vendedor basado en el id de su usuario en Laravel
        [HttpGet]
        public Object GetSellerData(long id)
        {
            return c.GetSellerData(id);
        }

        [HttpPost]
        public Object StoreContact(string test)
        {

            return "petición recibida con " + test;
        }

        
    }
}
