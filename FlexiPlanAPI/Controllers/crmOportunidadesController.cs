﻿using FlexiPlanAPI.Extensores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FlexiPlanAPI.Controllers
{
    public class crmOportunidadesController : ApiController
    {
       public  crmExtensores crm = new crmExtensores();

        [HttpGet]
        [AllowAnonymous]
        public List<VendedorP> Vendedores()
        {
            return crm.getEmpleados();
        }
        
        [HttpGet]
        [AllowAnonymous]
        public VendedorP GetVendedor(int idVendedor)
        {
            
            return crm.getVendedor(idVendedor);
        }


    }
    
}
