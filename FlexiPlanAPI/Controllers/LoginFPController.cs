﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FlexiPlanAPI.Extensores;
using FlexiPlanAPI.Models;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;

namespace FlexiPlanAPI.Controllers
{
    public class LoginFPController : ApiController
    {
        private Model db = new Model();
        LoginExt loginExt = new LoginExt();


        //Metodo que se encarga comprobar que toda la información coincida en la taba Usuario y PerfilEnc
        [HttpPost]
        [AllowAnonymous]
        public Object Login(string user, string pass, long idEmpresa)
        {
            if (IsValidEmail(user))
            {
               return loginExt.FindByEmail(user);
            }
            else if(loginExt.FindByUser(user)!=null && idEmpresa==1)
            {
                return loginExt.FindByUser(user);
            }
            else
            {
                return "Usuario No existe";
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public Object ResetPassword(string email, string password)
        {
            var UserToReset = loginExt.FindByEmail(email);

            Usuario usuario = new Usuario();

            usuario = db.Usuario.Find(UserToReset.id);

            usuario.clave = loginExt.bcrypt(password);

            db.SaveChanges();

            return true;
        }





        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }


    }
}

