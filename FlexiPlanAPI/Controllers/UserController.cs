﻿using FlexiPlanAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FlexiPlanAPI.Controllers
{
    public class UserController : ApiController
    {
        private Model db = new Model();

        public List<EmpresaP> GetUsuario()
        {
            List<EmpresaP> empresas = new List<EmpresaP>();
            var emp = db.Empresa.ToList();

            foreach (var item in emp)
            {
                empresas.Add(new EmpresaP { Id = item.Id, Nombre = item.EmpresaDes });
            }
            return empresas;

        }



    }
}
public class UsuarioP
{
    public long idTabla { get; set; }
    public string Name { get; set; }
    public string Pass { get; set; }
    public string email { get; set; }
}

public class EmpresaP
{
    public long Id { get; set; }
    public string Nombre { get; set; }
}
