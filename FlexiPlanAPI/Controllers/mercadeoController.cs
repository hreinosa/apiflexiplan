﻿using FlexiPlanAPI.Extensores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FlexiPlanAPI.Controllers
{
    public class mercadeoController : ApiController
    {
        public mercadeoExt mercadeo = new mercadeoExt();
        [HttpGet]
        public List<SucursalP> GetSucursales()
        {
            return mercadeo.GetSucursales();
        }
        [HttpGet]
        public List<CategoriaP> GetCategories()
        {
            return mercadeo.GetCategorias();
        }

        [HttpGet]
        public List<InvProductoP> GetProductsByCat(long id)
        {
            return mercadeo.GetProductos(id);
        }

        //NUEVO METODO PARA OBTENER UN PRODUCTO
        [HttpGet]
        public List<InvProductoP> GetProduct(long id, string codigo)
        {
            return mercadeo.GetProducto(id,codigo);
        }


        [HttpGet]
        public List<CategoriaP> GetCategoria(long id)
        {
            return mercadeo.GetCategoria(id);
        }

    }
}
